{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "devops.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Helm required labels */}}
{{- define "devops.labels" -}}
heritage: {{ .Release.Service }}
release: {{ .Release.Name }}
chart: {{.Chart.Name}}-{{.Chart.Version}}-{{.Chart.AppVersion}}
app: "{{ template "devops.name" . }}"
{{- end -}}

{{/* matchLabels */}}
{{- define "devops.matchLabels" -}}
release: {{ .Release.Name }}
app: "{{ template "devops.name" . }}"
{{- end -}}

{{- define "devops.apiserver.controller.loglevel" -}}
{{- $loglevel := .Values.global.logLevel -}}
{{- if eq $loglevel "debug" }}
    {{- printf "9" -}}
{{- else if eq $loglevel "info" }}
    {{- printf "5" -}}
{{- else if eq $loglevel "error" }}
    {{- printf "3" -}}
{{- else }}
    {{- printf "5" -}}
{{- end -}}
{{- end -}}

{{- define "devops.diabloFrontend.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.diabloFrontend.repository -}}
{{- $tag := .Values.global.images.diabloFrontend.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.docsx.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.docsx.repository -}}
{{- $tag := .Values.global.images.docsx.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.diabloFrontend.args" -}}
- '--log-level={{ .Values.global.logLevel }}'
- '--log-disable-color'
- '--log-enable-caller'
- '--path-prefix={{ .Values.alaudaConsole.pathPrefix }}'
- '--assets-host-path={{ .Values.alaudaConsole.assetsHostPath }}'
- '--oidc-issuer-url={{ .Values.alaudaConsole.oidcIssuerUrl }}'
- '--oidc-client-id={{ .Values.alaudaConsole.oidcClientId }}'
- '--oidc-client-secret={{ .Values.alaudaConsole.oidcClientSecret }}'
- '--oidc-redirect-url={{ .Values.alaudaConsole.oidcRedirectUrl }}/console-devops/'
- '--oidc-protocol-override={{ .Values.alaudaConsole.oidcProtocolOverride }}'
- '--api-address={{ .Values.alaudaConsole.apiAddress}}'
{{- end -}}

{{- define "devops.devopsApi.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.devopsApi.repository -}}
{{- $tag := .Values.global.images.devopsApi.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.alaudaConsole.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.alaudaConsole.repository -}}
{{- $tag := .Values.global.images.alaudaConsole.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.catalogController.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.catalogController.repository -}}
{{- $tag := .Values.global.images.catalogController.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.catalogControllerChartStore.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.catalogControllerChartStore.repository -}}
{{- $tag := .Values.global.images.catalogControllerChartStore.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.catalogControllerAsfChartStore.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.catalogControllerAsfChartStore.repository -}}
{{- $tag := .Values.global.images.catalogControllerAsfChartStore.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "devops.docs.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.docs.repository -}}
{{- $tag := .Values.global.images.docs.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "base.devopsApiServer.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.devopsApiServer.repository -}}
{{- $tag := .Values.global.images.devopsApiServer.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "base.devopsPipelineTemplates.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.devopsPipelineTemplates.repository -}}
{{- $tag := .Values.global.images.devopsPipelineTemplates.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}


{{- define "base.annotations" -}}
{{- printf "%s/product:  Alauda Devops" .Values.global.labelBaseDomain -}}
{{- end -}}

{{- define "apiserver.matchLabels" -}}
{{- printf "%s/devops-apiserver: 'true'" .Values.global.labelBaseDomain -}}
{{- end -}}

{{- define "controller.matchLabels" -}}
{{- printf "%s/devops-controller: 'true'" .Values.global.labelBaseDomain -}}
{{- end -}}