### 部署参数说明
Values.global.logLevel : 指定 devops-apiserver, devops-controller, devops-api, alauda-console的日志级别，在部署的时候，如果不填值，则默认为info,可选值为debug,info,error.

devops-apiserver和devops-controller 在其 deployment 中使用 -v 确定不同的日志级别， 现在的对应关系是 error:3 info:5 debug:9


### 部署后修改
部署后，如果想要修改组件的日志级别，可以直接修改各个组件对应的deployment

其中devops-apiserver, devops-controller中需要修改启动参数中的 -v 参数，参数值从 0 到 9, 数值越大,打印的日志会越多。

devops-api, alauda-console两个组件需要修改启动参数 --log-level  可选择的值为 debug、info、warn、error、fatal。其中debug级别会打印所有级别的日志，fatal只会打印fatal级别的日志