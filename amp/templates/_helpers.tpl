{{/* vim: set filetype=mustache: */}}

{{/*
Expand the name of the chart.
*/}}
{{- define "amp.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "amp.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- $projectName := default .Chart.Name .Values.global.projectName -}}
{{- printf "%s-%s" $name $projectName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "amp.logstash.ingress.fullname" -}}
{{ template "amp.fullname" . }}-logstash-ingress
{{- end -}}

{{- define "amp.logstash.service.fullname" -}}
{{ template "amp.fullname" . }}-logstash-service
{{- end -}}

{{- define "amp.logstash.statefulset.fullname" -}}
{{ template "amp.fullname" . }}-logstash-statefulset
{{- end -}}

{{/* Helm required labels */}}
{{- define "amp.labels" -}}
heritage: {{ .Release.Service }}
release: {{ .Release.Name }}
chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app: "{{ template "amp.name" . }}"
amp.alauda.io/metrics: kong
{{- end -}}

{{/* matchLabels */}}
{{- define "amp.matchLabels" -}}
release: {{ .Release.Name }}
app: "{{ template "amp.name" . }}"
{{- end -}}


{{/*
config amp controller
*/}}
{{- define "amp.controller.args" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- $projectName := default .Chart.Name .Values.global.projectName -}}
{{- $kongSerivceName := printf "%s-%s-kong" $name $projectName | trunc 63 | trimSuffix "-" -}}
{{- $adminURL := printf "http://%s:%.0f" $kongSerivceName .Values.service.ports.adminHttp.port -}}
{{- $adminURLSwagger := printf "https://%s:%.0f" .Values.kongForSwagger.serviceName .Values.kongForSwagger.adminPort -}}
{{- $proxyURL := printf "http://%s:%.0f" .Values.masterHost .Values.service.ports.proxyHttp.nodePort -}}
{{- $proxySecureURL := printf "https://%s:%.0f" .Values.masterHost .Values.service.ports.proxyHttps.nodePort -}}
{{- if and (and .Values.kong.deploy .Values.kong.exposeAsService) (le .Values.kong.replicas 1.0) -}}
- '--gateway-admin-server={{ $adminURL }}'
- '--gateway-proxy-server={{- $proxyURL -}}'
- '--gateway-proxy-secure-server={{- $proxySecureURL -}}'
{{- else -}}
- '--gateway-admin-server={{ .Values.kong.external.gatewayAdmin }}'
- '--gateway-proxy-server={{- .Values.kong.external.gatewayDomain -}}'
- '--gateway-proxy-secure-server={{- .Values.kong.external.gatewaySecureDomain -}}'
{{- end}}
- '--gateway-admin-server-test={{ $adminURLSwagger }}'
- '--logtostderr'
- '-v={{ .Values.ampController.logLevel }}'
{{- end -}}

{{/*
config amp metering
*/}}

{{- define "amp.metering.args" -}}
{{- $elasticURL := printf "http://%s:%.0f" .Values.env.elasticHost .Values.env.elasticPort -}}
- '--metering-standby=false'
- '--gateway-standby=true'
- '--elasticsearch-url={{ $elasticURL }}'
- '--elasticsearch-user={{ .Values.env.elasticUsername }}'
- '--elasticsearch-password={{ .Values.env.elasticPassword }}'
- '--lease-concurrence={{ .Values.ampMetering.concurrence }}'
- '--logtostderr'
- '-v={{ .Values.ampMetering.logLevel }}'
{{- end -}}

{{- define "amp.controller.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.controller.repository -}}
{{- $tag := .Values.global.images.controller.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "amp.logstash.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.logstash.repository -}}
{{- $tag := .Values.global.images.logstash.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}


{{/*
config amp dashboard
*/}}
{{- define "amp.dashboard.args" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- $projectName := default .Chart.Name .Values.global.projectName -}}
{{- $kongSerivceName := printf "%s-%s-kong" $name $projectName | trunc 63 | trimSuffix "-" -}}
{{- $adminURL := printf "http://%s:%.0f" $kongSerivceName .Values.service.ports.adminHttp.port -}}
{{- $proxyURL := printf "http://%s:%.0f" .Values.masterHost .Values.service.ports.proxyHttp.nodePort -}}
{{- $proxySecureURL := printf "https://%s:%.0f" .Values.masterHost .Values.service.ports.proxyHttps.nodePort -}}
{{- $proxyURLSwagger := printf "http://%s:%.0f" .Values.masterHost .Values.kongForSwagger.gatewayDomainPort -}}
{{- $proxySecureURLSwagger := printf "https://%s:%.0f" .Values.masterHost .Values.kongForSwagger.gatewaySecureDomainPort -}}
{{- if and (and .Values.kong.deploy .Values.kong.exposeAsService) (le .Values.kong.replicas 1.0) -}}
- '--gateway-domain={{ $proxyURL }}'
- '--gateway-secure-domain={{ $proxySecureURL }}'
{{- else -}}
- '--gateway-domain={{ .Values.kong.external.gatewayDomain }}'
- '--gateway-secure-domain={{ .Values.kong.external.gatewaySecureDomain }}'
{{- end}}
- '--gateway-domain-test={{ $proxyURLSwagger }}'
- '--gateway-secure-domain-test={{ $proxySecureURLSwagger }}'
- '--enable-anonymous={{ .Values.ampDashboard.enableAnonymous }}'
- '--console-url={{ .Values.ampDashboard.consoleURL }}'
- '--console-org={{ .Values.ampDashboard.consoleOrg }}'
- '--path-prefix={{ .Values.ampDashboard.pathPrefix }}'
- '--logtostderr'
- '-v={{ .Values.ampDashboard.logLevel }}'
{{- end -}}

{{- define "amp.dashboard.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.dashboard.repository -}}
{{- $tag := .Values.global.images.dashboard.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "amp.dashboard.minioEndpointInner" -}}
{{- $serviceName := .Values.env.minio.serviceName -}}
{{- $port := .Values.env.minio.servicePort -}}
{{- printf "%s:%s" $serviceName $port -}}
{{- end -}}



{{/*
config kong database
*/}}
{{- define "amp.database.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.database.repository -}}
{{- $tag := .Values.global.images.database.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "amp.database.nodeSelector" -}}
{{- if .Values.kong.database.nodeSelector }}
nodeSelector:
{{ toYaml .Values.kong.database.nodeSelector | indent 2 }}
{{- else }}
{{- if .Values.kong.database.persistence.host.nodeName }}
nodeSelector:
  kubernetes.io/hostnakong.envme: {{ .Values.kong.database.persistence.host.nodeName }}
{{- end }}
{{- end }}
{{- end }}

{{- define "amp.database.volume" -}}
{{- if .Values.kong.database.persistence.enabled }}
persistentVolumeClaim:
  claimName: {{ .Values.kong.database.persistence.existingClaim | default (printf "%s-database" (include "amp.fullname" .)) }}  
{{- else }}
{{- if and (.Values.kong.database.persistence.host.nodeName) (.Values.kong.database.persistence.host.path) }}
hostPath:
  path: {{ .Values.kong.database.persistence.host.path }}
  type: DirectoryOrCreate
{{- else }}
emptyDir: {}
{{- end }}
{{- end }}
{{- end }}

{{- define "amp.database.password" -}}
  {{- if .Values.kong.database.deploy -}}
    {{- .Values.kong.database.postgresPassword | b64enc | quote -}}
  {{- else -}}
    {{- .Values.kong.database.external.postgresPassword | b64enc | quote -}}
  {{- end -}}
{{- end -}}



{{/*
config kong
*/}}

{{- define "amp.kong.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.kong.repository .Values.global.images.kong.tag -}}
{{- end -}}

{{- define "amp.init_kong.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.init_kong.repository .Values.global.images.init_kong.tag -}}
{{- end -}}

{{- define "amp.kong.switch.database" -}}
{{- if and .Values.kong.database.deploy .Values.kong.database.exposeAsService }}
- name: KONG_DATABASE
  value: postgres
- name: KONG_PG_HOST
  value: {{ template "amp.fullname" . -}}-kong-database
- name: KONG_PG_DATABASE
  value: {{ .Values.kong.database.postgresDatabase }}
- name: KONG_PG_USER
  value: {{ .Values.kong.database.postgresUser }}
- name: KONG_PG_PORT
  value: "5432"  
{{- else}}
- name: KONG_DATABASE
  value: {{ .Values.kong.database.external.database }}
- name: KONG_PG_HOST
  value: {{ .Values.kong.database.external.host }}
- name: KONG_PG_DATABASE
  value: {{ .Values.kong.database.external.postgresDatabase }}
- name: KONG_PG_USER
  value: {{ .Values.kong.database.external.postgresUser }}
- name: KONG_PG_PORT
  value: {{ .Values.kong.database.external.port | quote }}
{{ end }}
- name: KONG_PG_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ template "amp.fullname" . -}}-secret
      namespace: {{ .Values.global.namespaces }}
      key: db-password
{{- end -}}

{{- define "amp.kong.wait-for-db" -}}
- name: wait-for-db
  image: {{ template "amp.init_kong.image" . }}
  env:
{{- include "amp.kong.switch.database" .  | nindent 2 }}
{{- include "amp.kong.env" .  | nindent 2 }}
  command: [ "/bin/sh", "/wait-for-db-and-plugin.sh"]
{{- end -}}

{{- define "amp.kong.env" -}}
{{- range $key, $val := .Values.kong.env }}
- name: KONG_{{ $key | upper}}
{{- $valueType := printf "%T" $val -}}
{{ if eq $valueType "map[string]interface {}" }}
{{ toYaml $val | indent 2 -}}
{{- else }}
  value: {{ $val | quote -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "logstash.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "logstash.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "logstash.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "amp.logstash.serviceAccountName" -}}
{{- if .Values.logstash.serviceAccount.create -}}
    {{ default (include "amp.fullname" .) .Values.logstash.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.logstash.serviceAccount.name }}
{{- end -}}
{{- end -}}

{{/*
files-config name
*/}}
{{- define "amp.logstash.config.files" -}}
{{ template "amp.fullname" . }}-files
{{- end -}}

{{/*
patterns-config name
*/}}
{{- define "amp.logstash.config.patterns" -}}
{{ template "amp.fullname" . }}-patterns
{{- end -}}

{{/*
pipeline-config name
*/}}
{{- define "amp.logstash.config.pipeline" -}}
{{ template "amp.fullname" . }}-pipeline
{{- end -}}

{{/*
Return the appropriate apiVersion for deployment.
*/}}
{{- define "amp.deployment.apiVersion" -}}
{{- if semverCompare ">=1.4-0, <1.7-0" .Capabilities.KubeVersion.GitVersion -}}
"extensions/v1beta1"
{{- else if semverCompare "^1.7-0" .Capabilities.KubeVersion.GitVersion -}}
"apps/v1"
{{- end -}}
{{- end -}}

{{/*
Return the appropriate apiVersion for statefulset.
*/}}
{{- define "amp.statefulset.apiVersion" -}}
{{- if semverCompare ">=1.4-0, <1.7-0" .Capabilities.KubeVersion.GitVersion -}}
"apps/v1beta2"
{{- else if semverCompare "^1.7-0" .Capabilities.KubeVersion.GitVersion -}}
"apps/v1"
{{- end -}}
{{- end -}}
