#/bin/bash

helm install stable3/amp-kong \
	     --name amp-kong \
	     --version=v2.6.1 \
	     --namespace alauda-system \
	     --set global.registry.address=index.alauda.cn \
	     --set postgresql.enabled=false \
	     --set env.pg_host=10.0.128.152 \
	     --set env.pg_database=kongswagger \
	     --set env.pg_port=5432 \
	     --set env.pg_user=alauda \
	     --set env.pg_password=sgOnxrI9WN638Hj8 \
	     --dry-run \
	     --debug > 1.out

helm install ./amp-kong \
	     --name amp-kong \
	     --version=v2.8-b.1 \
	     --namespace other-system \
	     --set global.registry.address=index.alauda.cn \
	     --set postgresql.enabled=false \
	     --set env.pg_host=10.0.128.107 \
	     --set env.pg_database=kongswagger \
	     --set env.pg_port=5432 \
	     --set env.pg_user=kong \
	     --set env.pg_password= # \
	     --dry-run \
	     --debug > 1.out
