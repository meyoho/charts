#!/bin/bash

helm install release/amp \
        --name amp \
        --set masterHost=10.0.128.241 \
        --set global.namespaces=alauda-system \
        --namespace alauda-system \
        --set defaultRootName=alauda \
        --set global.registry.address=index.alauda.cn \
        --set global.images.logstash.repository=alaudak8s/logstash-oss \
        --set ampDashboard.consoleOrg=alauda \
        --set ampDashboard.consoleURL=http://icarus:8080 \
        --set logstash.serviceAccount.create=false \
        --set service.ports.proxyHttp.nodePort=30444 \
        --set service.ports.proxyHttps.nodePort=30443 \
        --set env.elasticHost="cpaas-elasticsearch" \
        --set env.elasticPort="9200" \
        --set env.elasticUsername="alaudaes" \
        --set env.elasticPassword="QtAWihNGea" \
        --set env.acpConfigMapNamespace="cpaas-system" \
        --set amp.deploy=true \
        --set kong.deploy=true \
        --set kong.replicas=1 \
        --set kong.exposeAsService=true \
        --set kong.database.deploy=true \
        --set kong.database.exposeAsService=true \
        --set kong.runMigrations=true \
        --set kong.env.db_update_frequency=5 \
        --dry-run \
        --debug > 1.out