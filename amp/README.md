# values 变量说明
```yaml
masterHost: 0.0.0.0 # 目标主机地址，如果使用内置kong则必填
nameOverride: ""
resources:
  requests:
    memory: 300Mi
    cpu: 100m
  limits:
    memory: 1Gi
    cpu: 1
ampController:
  replicas: 2
  logLevel: 8  # amp-controller日志等级
ampDashboard:
  replicas: 2
  # amp dashboard默认参数
  insecurePort: 9090
  port: 8443 
  insecureBindAddress: 0.0.0.0
  bindAddress: 0.0.0.0
  defaultCertDir: /certs
  certFile: ""
  keyFile: ""
  apiserverHost: ""
  kubeConfigFile: ""
  autoGenerateCertificates: false
  enableTheme: true
  ace: http://jakiro.alauda-system
  enableAnonymous: false
  consoleOrg: alauda
  consoleURL: https://0.0.0.0/console
  logLevel: 8
  containerPort: 9090
  nodePort: 32305

kong:
  replicas: 1                                       
  deploy: true                                     # 部署kong
  exposeAsService: true                            # 是否用K8S Svc来构建访问地址
  runMigrations: true                              # 运行Migrations，将会初始化数据库
  env:
    proxy_access_log: /dev/stdout                  # kong的环境变量
    admin_access_log: /dev/stdout
    admin_gui_access_log: /dev/stdout
    portal_api_access_log: /dev/stdout
    proxy_error_log: /dev/stderr
    admin_error_log: /dev/stderr
    admin_gui_error_log: /dev/stderr
    portal_api_error_log: /dev/stderr
    admin_lister: 0.0.0.0:8001, 0.0.0.0:8444 ssl
  database:
    replicas: 1
    # if external Postgresql is used, set "deploy" to "false"
    # and fill the connection informations in "external" section.
    # or Postgresql will be deployed
    deploy: true                                   # 部署kong
    exposeAsService: true                          # 是否用K8S Svc来构建访问地址
    postgresUser: kong
    postgresPassword: kong
    postgresDatabase: kong
    postgresInitdbArgs:
    # resources:
    #   requests:
    #     cpu: 512m
    #     memory: 1Gi
    affinity: {}
    tolerations: []
    nodeSelector: *nodeSelector 
    persistence:
      enabled: *persistenceEnabled
      existingClaim: *existingClaim
      storageClass: *storageClass
      accessMode: *accessMode
      size: 10Gi
    ## host is used only when persistence.enabled=false
    ## this is useful to persist data without a StorageClass or PVC available
    ## nodeName and path MUST all be set      
      host:
        nodeName: *nodeName
        path: /tmp/harbor/jobservice
    external:                                         # kong的外置数据库配置，需要将kong.database.deploy设置为false
      host: "192.168.0.10"
      port: 5432
      postgresUser: kong
      postgresPassword: kong
      postgresDatabase: kong
      database: postgres                              # 数据库类型
  external:                                           # 外置kong配置，需要将kong.deploy设置为false
    gatewayAdmin: http://10.104.231.49:31666          # kong的admin HTTP协议地址，需要加上协议
    gatewaySecureAdmin: https://10.104.231.49:31666   # kong的admin HTTPS协议地址，需要加上协议
    gatewayDomain: http://10.104.231.49:31666         # kong的proxy HTTP协议地址，需要加上协议
    gatewaySecureDomain: https://10.104.231.49:31666  # kong的proxy HTTPS协议地址，需要加上协议

service:                                              # 使用内置kong服务的端口地址
  name: kong
  ports:
    adminHttp:
      port: 8001
      nodePort: 30001                                 # admin HTTP协议nodePort端口
    adminHttps:
      port: 8444                                                                                        
      nodePort: 30444                                 # admin HTTPS协议nodePort端口
    proxyHttp:
      port: 8000                                                                                        
      nodePort: 30000                                 # proxy HTTP协议nodePort端口
    proxyHttps:
      port: 8443                                                                                        
      nodePort: 30443                                 # proxy HTTPS协议nodePort端口
      
```