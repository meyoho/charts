# Welcome to [sentry](https://bitbucket.org/mathildetech/sentry) 👋

## charts主要参数说明
```
global:
  registry:
    address: index.alauda.cn                        镜像仓库
  namespace: alauda-system                          部署的目标 namespace
args:
  syncPeriod: 7200                                  安装更新定时同步时间，单位秒
  workers: 1                                        安装更新的任务worker数量
```
