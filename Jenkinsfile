// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
// image can be used for promoting...
def BOTNAME = "devops-chat-bot"
def charts
def release
def failedDownloads = []
def uploads = [:]
def chartDeploy
def customMsg = ""
def missingComponents = ""
def scmVars = null
// All chart folders
// ATTENTION: Adding a new chart should change this variable
def ALL_CHARTS = [
	"dashboard",
	"devops",
	"e2e-test/asm-demo",
	"e2e-test/devops-demo",
	"e2e-test/dashboard-e2e",
	"e2e-test/artemis-e2e",
	"dex",
	"portal",
	"aml-core",
	"global-aml",
	"asf",
	"appcore",
	"rook-ceph",
	"ceph-rook-cluster",
	"cephfs-provisioner",
	"csp-provisioner",
	"nfs-client-provisioner",
	"nfs-server-provisioner",
	"asm",
	"jaeger-operator",
	"istio",
	"cluster-asm",
	"asm-init",
	"jenkins",
	"gitlab-ce",
	"harbor",
	"global-asm",
	"ti-matrix-installer",
	"flagger"
]

def IGNORE_DEPLOY= [
	"jenkins-updatecenter": true,
	"alauda-common-component": true,
	"alauda-common-pipeline": true,
	"alauda-common-secret": true,
	"cpaas-appreleases": true,
	"asm-operator": true,
	"athens": true
]

pipeline {
	agent { label 'base' }

	options {
		// 保留多少流水线记录（建议不放在jenkinsfile里面）
		buildDiscarder(logRotator(numToKeepStr: '50'))
		timeout(time: 3, unit: 'HOURS')

		// 不允许并行执行
		disableConcurrentBuilds()

	}

	parameters {
		string(name: 'CHART', defaultValue: '', description: 'The chart that is been worked on.')
		string(name: 'VERSION', defaultValue: '', description: 'New version of the chart.')
		string(name: 'COMPONENT', defaultValue: '', description: 'The component of the chart worked on. Will be used to update the values.yaml file.')
		string(name: 'IMAGE_TAG', defaultValue: '', description: 'The tag used for the new version of the component.')
		string(name: 'ENV', defaultValue: 'test', description: 'Environment used to publish the chart.')
		booleanParam(name: 'DEBUG', defaultValue: true, description: 'Debugging a pipeline will not cause real changes in the repo or in the chart repository.')
		booleanParam(name: 'STABLE', defaultValue: false, description: 'Is a stable version publishing.')
		string(name: 'GIT_TAG', defaultValue: '', description: 'The tag of the chart.')
		string(name: 'BRANCH', defaultValue: 'master', description: 'Branch to be used by the chart.')
		string(name: 'PR_CHANGE_BRANCH', defaultValue: 'master', description: 'Branch that which merge from when trigger by a pr change')
		string(name: 'PRODUCT', defaultValue: 'acp', description: 'The product , it supports acp or aml now')
		text(name: 'DATA', defaultValue: '', description: 'optional data used to trigger chart')
	}
	environment {
		TAG_CREDENTIALS = "daniel-bitbucket"
		OWNER = "mathildetech"
		REPOSITORY = "charts"
		TEST = "chartmuseum-test"
		INCUBATOR = "chart-incubator-address"
		STABLE = "chart-repository-address"
		RELEASE = "chart-release-address"
		CHARTMUSEUM_CREDENTIALS = "chart-repository"
		TEST_PIPELINE = "/automation/automation-apitest"
	}
	// stages
	stages {
		stage('Checkout') {
			steps {
				script {
					// checkout code
					container('tools') {
						retry(3) { scmVars = checkout scm }
						release = deploy.chartsRelease(
							params.CHART,
							params.COMPONENT,
							params.VERSION,
							params.IMAGE_TAG,
							params.BRANCH,
							params.GIT_TAG,
							params.STABLE,
							params.PR_CHANGE_BRANCH
							)
						release.product = params.PRODUCT
						release.debug = params.DEBUG
						release.verbose = true
						release.setScm(scmVars)
						release.optionalTriggerData(params.DATA)
						release.calculate()

						echo """release data: ${release}
														chart: ${release.chart}				component: ${release.component}
														chartVersion: ${release.chartVersion}  imageTag: ${release.imageTag}
														branch: ${release.branch}			  gitTag: ${release.gitTag}
														stable: ${release.stable}			  debug: ${release.debug}
														verbose: ${release.verbose}

														checkoutBranch: ${release.checkoutBranch}  genVersion: ${release.genVersion}
														versionGenSubcommand: ${release.versionGenSubcommand}  generateBasedVersion: ${release.generateBasedVersion}
														triggerTest: ${release.triggerTest}  shouldCommit: ${release.shouldCommit}
														shouldTag: ${release.shouldTag}  useWhilteList: ${release.useWhilteList}
														oneChart: ${release.oneChart}  versionPrefix: ${release.versionPrefix}
														chartmuseumAddCredentials: ${release.chartmuseumAddCredentials}  chartmuseumUserpassCredentials: ${release.chartmuseumUserpassCredentials}
														smokingTags ${release.smokingTags()}
												"""
						echo "scm data: ${scmVars}"

						// extract git information
						echo "current branch: ${scmVars.GIT_BRANCH}, current commmit: ${scmVars.GIT_COMMIT}"
						if (!release.debug) {
							if (release.checkoutBranch != "") {
								echo "checking out ${release.checkoutBranch}"
								sh "git checkout ${release.checkoutBranch}"
							}
						}

					}

					echo "Parameters: chart: ${release.chart}  version: ${release.version}  component: ${release.component}  tag: ${release.imageTag}"
				}
			}
		}
		stage("Updating chart version") {
			when {
				expression { !release.stable }
			}
			steps {
				script {
					container('tools') { release.updateChart() }
				}
			}
		}
		stage('Bundle charts') {
			when { expression { release.shouldBundleCharts } }
			steps {
				script {
					container('tools') {
						echo "Bundling charts..."
						release.bundleCharts(ALL_CHARTS)
						echo "Uploading charts..."
						release.uploadCharts()
					}
				}
			}
		}
		stage('Commit repo') {
			when { expression { release.shouldCommit } }
			steps {
				script {
					container('tools') {
						deploy.gitCommit(
							TAG_CREDENTIALS,
							release.tagName(),
							OWNER,
							REPOSITORY,
							"bitbucket.org",
							release.commitMessage()
							)
					}
				}
			}
		}
		stage('Deploy'){
			when { expression { release.shouldDeployForSmoke && !IGNORE_DEPLOY[params.CHART]  } }
			steps{
				script {
					container("tools"){
						def toEnv = release.environment
						// toEnv = "int" // always be int now
						chartDeploy = deploy.chartsDeploy(release.chart, release.version).to(release.environment, release.environmentData)
						chartDeploy.deploy()
					}
				}
			}
		}
		stage('Trigger Testing') {
			when { expression { release.shouldTriggerTest && !IGNORE_DEPLOY[params.CHART] && !(release.chart && !release.component) } }
			parallel {
				stage("API TEST") {
					when{
						expression {
							!(release.component in ["underlord", "icarus", "diablo", "mephisto"]) || !release.component
						}
					}
					steps {
						script {
							container('tools') {
								def timeoutTime = 30
								if (release.shouldUploadToStable) {
									timeoutTime = 180
								}
								timeout(time: timeoutTime, unit: 'MINUTES'){
									deploy.triggerAuto([
										env: release.environment,
										caseType: release.component,
										branch: branch,
										pipeline: "apitest",
									]).addPostAction({ succeeded ->
										if (!succeeded && chartDeploy) {
											echo "API Testing Failed, will rollback environment..."
											chartDeploy.rollback()
											return "Automated test failed..."
										}
										return ""
									}).start()
								}
							}
						}
					}
				}
				stage("UI TEST") {
					when{
						expression {
							release.component in ["underlord", "icarus", "diablo", "mephisto"] || !release.component
						}
					}
					steps {
						script {
							container('tools') {
								def timeoutTime = 1
								if (release.shouldUploadToStable) {
									timeoutTime = 3
								}
								timeout(time: timeoutTime, unit: 'HOURS'){
									deploy.triggerAuto([
										env: release.environment,
										caseType: release.component,
										branch: branch,
										pipeline: "ui-test",
									]).addPostAction({ succeeded ->
										if (!succeeded && chartDeploy) {
											echo "UI Testing Failed, will rollback environment..."
											chartDeploy.rollback()
											return "Automated test failed..."
										}
										return ""
									}).start()
								}
							}
						}
					}
				}
			}
		}
		stage('Upload to stable') {
			when { expression { release.shouldUploadToStable } }
			steps {
				script {
					echo "uploading to stable charts in staging environment"
					container('tools') {
						def subFolder = "tmp-folder"
						failedDownloads = release.fetchEnvCharts(release.environment, subFolder)
						uploads = release.uploadCharts(subFolder)
					}
				}
			}
		}
		stage('Tag repo') {
			when { expression { release.shouldTag } }
			steps {
				script {
					container('tools') {
						def tag = release.tagName()
						deploy.gitLightweightTag(
							TAG_CREDENTIALS,
							release.tagName(),
							OWNER,
							REPOSITORY
							)
					}
				}
			}
		}

		stage('Trigger stable test'){
			when {
				expression { release.triggerTest }
			}
			steps {
				script {
					container('tools') {
						catchError(buildResult: 'SUCCESS', message: "Checking rc versions") {
							echo "checking for branch ${release.branch} using version prefix: ${release.versionPrefix} ..."
							charts = deploy.charts(release.chartmuseumAddCredentials, release.chartmuseumUserpassCredentials, "chartcode.yaml", release.verbose)
							missingComponents = charts.getMissingVersions(release.generateBasedVersion)
							if (missingComponents != null && missingComponents.size() == 0 && !release.debug) {
								echo "all components are already built. will trigger stable test..."
								// try {
								// 	build job: '/common/common-ake-integration-test', parameters: [
								// 		[$class: 'StringParameterValue', name: 'BRANCH', value: release.branch],
								// 		[$class: 'StringParameterValue', name: 'VERSION_PREFIX', value: release.versionPrefix],
								// 	], wait: false
								// } catch (Exception exc) {
								// 	build job: '/ake-integration-test', parameters: [
								// 		[$class: 'StringParameterValue', name: 'BRANCH', value: release.branch],
								// 		[$class: 'StringParameterValue', name: 'VERSION_PREFIX', value: release.versionPrefix],
								// 	], wait: false
								// }
							} else {
								echo "still waiting some components: ${data}. skip"
							}
						}
					}

				}
			}
		}
	}

	// (optional)
	// happens at the end of the pipeline
	post {
		// 成功
		always {
			script {
				if (!release.debug) {
					if (failedDownloads) {
						customMsg = "⚠️**Failed downloads from incubator**⚠️:\n *(these charts are deploy on staging but not in **incubator**)*: ${failedDownloads}"
					}
					if (uploads) {
						customMsg = "${customMsg}\n**Upload results**:"
						if (uploads.failed) {
							customMsg = "${customMsg}\n❗️*Failed uploads*❗️:\n${uploads.failed}"
						}
						if (uploads.succeeded) {
							customMsg = "${customMsg}\n*Successful uploads*:\n${uploads.succeeded}"
						}
					}
					if (missingComponents) {
						customMsg = "${customMsg}\n**Did not trigger to release chart: 缺少chart/组件: ${missingComponents}"
					}
					if (currentBuild.result == "SUCCESS") {
						deploy.notificationSuccess("Chart ${release.branch}-${release.tagName()}", '', "发布成功了  ${release.commitMessage()}", "${release.tagName()}", customMsg)
					} else {
						deploy.notificationFailed("Chart ${release.branch}-${release.tagName()}", '', "发布失败了  ${release.commitMessage()}", "${release.tagName()}", false, customMsg)
					}
					
				}
				try {
					deleteDir()
				} catch (Exception exc) {
					echo "error removing workspace: ${exc}"
				}
			}
		}
	}
}

