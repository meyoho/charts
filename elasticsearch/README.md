elasticsearch:
  es_user: alaudaes                             ## es 用户名
  es_passwd: es_password_1qaz2wsx               ## es 密码
  es_user_64: YWxhdWRhZXMK                      ## base64 加密过的 es 用户名
  es_passwd_64: ZXNfcGFzc3dvcmRfMXFhejJ3c3gK    ## base64 加密过的 es 密码
  node_ip1: 1.1.1.1                             ## 运行es、kafka 和 zk 这三个组件的第1个 node 的 ip
  node_ip2: 2.2.2.2                             ## 运行es、kafka 和 zk 这三个组件的第2个 node 的 ip
  node_ip3: 3.3.3.3                             ## 运行es、kafka 和 zk 这三个组件的第3个 node 的 ip
