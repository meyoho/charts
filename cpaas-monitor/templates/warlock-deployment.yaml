apiVersion: apps/v1
kind: Deployment
metadata:
  name: warlock
  labels:
    chart: '{{.Chart.Name}}'
    service_name: warlock
  namespace: {{ .Values.global.namespace }}
spec:
  replicas: {{ .Values.warlock.replicas }}
  revisionHistoryLimit: 5
  selector:
    matchLabels:
      service_name: warlock
  strategy:
    rollingUpdate:
      maxSurge: 0
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      labels:
        service_name: warlock
        version: v1
        app: warlock
        chart: '{{.Chart.Name}}'
      namespace: {{ .Values.global.namespace }}
    spec:
      affinity:
        podAffinity: {}
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchExpressions:
                  - key: service_name
                    operator: In
                    values:
                      - warlock
              topologyKey: kubernetes.io/hostname
      containers:
      - command:
        - /warlock/manager
        - --enable-leader-election
        - --grafana-url={{ .Values.grafana.url }}
        - --grafana-username={{ .Values.grafana.username }}
        - --grafana-password={{ .Values.grafana.password }}
        image: {{ .Values.global.registry.address }}/{{ .Values.global.images.warlock.repository }}:{{ .Values.global.images.warlock.tag }}
        imagePullPolicy: Always
        ports:
          - containerPort: 8080
            protocol: TCP
        resources:
{{ toYaml .Values.warlock.resources | indent 10 }}
        livenessProbe:
          failureThreshold: 2
          httpGet:
            path: /metrics
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 100
          periodSeconds: 2
          successThreshold: 1
          timeoutSeconds: 15
        name: warlock
        readinessProbe:
          failureThreshold: 3
          httpGet:
            path: /metrics
            port: 8080
            scheme: HTTP
          periodSeconds: 5
          successThreshold: 1
          timeoutSeconds: 15
        volumeMounts:
        - mountPath: /warlock/datasources/
          name: datasources
          readOnly: true
      restartPolicy: Always
      volumes:
        - configMap:
            defaultMode: 420
            name: warlock-grafana-datasources-configmap
          name: datasources
