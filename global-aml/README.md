# Heml Chart for AML global cluster

## 介绍

global-aml用于aml的global集群部署

## 环境要求

- k8s >= 1.13
- k8s Ingress Controller

## 安装charts

```
$ helm install . --name aml --namespace alauda-system \
--set global.ingress.host=${ingresshost} \
--set global.registry.address=${regAddress} 
```


## 卸载charts

```
$ helm delete aml --purge & \
kubectl delete clusterrolebindings.rbac.authorization.k8s.io "global-aml:${namespace}:cluster-admin"
```

## 配置

可以到 [values.yaml](values.yaml) 中查看所有配置的默认值, 使用 `helm install` 的 `--set key = value [, key = value]` 参数设置每个参数.

或者, 可以在安装 `chart` 时提供指定参数值的 `YAML` 文件. 例如:

```
$ helm install --name my-release -f values.yaml .
```


```
alaudaConsole:
  apiAddress: https://129.28.182.197
  oidcIssuerUrl: https://129.28.182.197/dex
  oidcRedirectUrl: https://129.28.182.197
  oidcClientId: alauda-auth
  oidcClientSecret: ZXhhbXBsZS1hcHAtc2VjcmV0
  oidcProtocolOverride: nul
```

## 备注
1. ingress查询
```
$ cat /etc/kubernetes/kubelet.conf | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
```