{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}

{{- define "amlbackend.image" -}}
{{- $repositoryName := .Values.global.images.amlbackend.repository -}}
{{- $tag := .Values.global.images.amlbackend.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "malthael.image" -}}
{{- $repositoryName := .Values.global.images.malthael.repository -}}
{{- $tag := .Values.global.images.malthael.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "console.image" -}}
{{- $repositoryName := .Values.global.images.alaudaConsole.repository -}}
{{- $tag := .Values.global.images.alaudaConsole.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "amlopr.image" -}}
{{- $repositoryName := .Values.global.images.amlopr.repository -}}
{{- $tag := .Values.global.images.amlopr.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "docs.image" -}}
{{- $repositoryName := .Values.global.images.amldocs.repository -}}
{{- $tag := .Values.global.images.amldocs.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "amlfilebrowser.image" -}}
{{- $repositoryName := .Values.global.images.amlfilebrowser.repository -}}
{{- $tag := .Values.global.images.amlfilebrowser.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "tensorboardui.image" -}}
{{- $repositoryName := .Values.global.images.tensorboard.repository -}}
{{- $tag := .Values.global.images.tensorboard.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "jenkinstool.image" -}}
{{- $repositoryName := .Values.global.images.jenkinsTool.repository -}}
{{- $tag := .Values.global.images.jenkinsTool.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "jnlp.image" -}}
{{- $repositoryName := .Values.global.images.jnlp.repository -}}
{{- $tag := .Values.global.images.jnlp.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "aml.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Helm required labels */}}
{{- define "aml.labels" -}}
heritage: {{ .Release.Service }}
release: {{ .Release.Name }}
chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app: "{{ template "aml.name" . }}"
mode: global
{{- end -}}

{{/* matchLabels */}}
{{- define "aml.matchLabels" -}}
release: {{ .Release.Name }}
app: "{{ template "aml.name" . }}"
mode: global
{{- end -}}




