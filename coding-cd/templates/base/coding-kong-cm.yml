---
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: {{ .Values.global.namespace }}
  name: e-coding-kong-conf
data:
  kong.conf: |
    #------------------------------------------------------------------------------
    # GENERAL
    #------------------------------------------------------------------------------
    #prefix = /usr/local/kong/
    log_level = notice
    proxy_access_log = logs/access.log
    proxy_error_log = logs/error.log
    admin_access_log = logs/admin_access.log
    admin_error_log = logs/error.log
    plugins = cors, coding-session-handler, signing, project-mapping
    anonymous_reports = off
    
    #------------------------------------------------------------------------------
    # NGINX
    #------------------------------------------------------------------------------
    proxy_listen = 0.0.0.0:80, 0.0.0.0:8443 ssl
    #stream_listen = off
    admin_listen = 127.0.0.1:8001, 127.0.0.1:8444 ssl
    #nginx_user = nobody nobody
    nginx_worker_processes = 8
    nginx_daemon = off
    #mem_cache_size = 128m
    #upstream_keepalive = 60
    #real_ip_header = X-Real-IP
    #real_ip_recursive = off
    #client_max_body_size = 0
    #client_body_buffer_size = 8k
    #error_default_type = text/plain
    
    #------------------------------------------------------------------------------
    # DATASTORE
    #------------------------------------------------------------------------------
    database = off
    declarative_config = /etc/kong/kong.yml
    #------------------------------------------------------------------------------
    # DNS RESOLVER
    #------------------------------------------------------------------------------
    #dns_resolver =
    #dns_hostsfile = /etc/hosts
    #dns_order = LAST,SRV,A,CNAME
    #dns_valid_ttl =
    #dns_stale_ttl = 4
    #dns_not_found_ttl = 30
    #dns_error_ttl = 1
    #dns_no_sync = off
    
    #------------------------------------------------------------------------------
    # TUNING & BEHAVIOR
    #------------------------------------------------------------------------------
    #router_consistency = strict
    
    #------------------------------------------------------------------------------
    # DEVELOPMENT & MISCELLANEOUS
    #------------------------------------------------------------------------------
    #lua_ssl_trusted_certificate =
    #lua_ssl_verify_depth = 1
    #lua_package_path = ./?.lua;./?/init.lua;
    #lua_package_cpath =
    #lua_socket_pool_size = 30

---
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: {{ .Values.global.namespace }}
  name: e-coding-kong-yaml
data:
  kong.yml: |
    _format_version: "1.1"
    
    services:
    - name: qflow
      url: http://e-cd-qflow-svc
      routes:
      - paths:
        - /api/user/(?<user>[^/]+)/project/(?<project>[^/]+)/qflow
        strip_path: false
      plugins:
      - name: signing
        config:
          auth_token: a780416c-5d27-4166-a494-5eb6d75ca3b0
    - name: cd
      url: http://e-cd-api-server-svc
      routes:
      - paths:
        - /api/user/(?<user>[^/]+)/project/(?<project>[^/]+)/cd
        strip_path: false
      plugins:
      - name: signing
        config:
          auth_token: 224dc6f5-1c05-4c64-91b5-e91baa31002f
    - name: qtap
      url: http://qtap-apigw.qtap:8000
      routes:
      - paths:
        - /api/qta/
        strip_path: true
      plugins:
      - name: signing
        config:
          auth_token: 5d99138d-c12a-45a3-9ad6-4786965c56fe
    
    plugins:
    - name: coding-session-handler
      config:
        session_service_url: http://e-coding-grpc-gateway-svc:20153/SessionService/getUser
        session_name: eid
        session_service_timeout: 3000
        redis_host: redis
        redis_port: 6379
        redis_database: 3
        redis_timeout: 2000
        redis_keepalive_idle_timeout: 10000
        redis_keepalive_pool_size_per_worker: 10
        session_key_expired_time: 86400

---
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: {{ .Values.global.namespace }}
  name: e-coding-kong-custom-nginx-template
data:
  custom_nginx.template: |
    # This is a custom nginx configuration template for Kong specs

    > if nginx_user then
    user {{ .Values.global.NGINX_USER }};
    > end
    worker_processes {{ .Values.global.NGINX_WORKER_PROCESSES }};
    daemon {{ .Values.global.NGINX_DAEMON }};
    
    pid pids/nginx.pid; # mandatory even for custom config templates
    error_log logs/error.log {{ .Values.global.LOG_LEVEL }};
    
    events {}
    
    http {
        log_format timed_combined '$remote_addr - $remote_user [$time_local]  '
                                  '"$request" $status $body_bytes_sent '
                                  '"$http_referer" "$http_user_agent" $request_time';
    
    > if #proxy_listeners > 0 or #admin_listeners > 0 then
        charset UTF-8;
    
        error_log logs/error.log {{ .Values.global.LOG_LEVEL }};
    
    > if anonymous_reports then
        {{ .Values.global.SYSLOG_REPORTS }}
    > end
    
    > if nginx_optimizations then
    >-- send_timeout 60s;          # default value
    >-- keepalive_timeout 75s;     # default value
    >-- client_body_timeout 60s;   # default value
    >-- client_header_timeout 60s; # default value
    >-- tcp_nopush on;             # disabled until benchmarked
    >-- proxy_buffer_size 128k;    # disabled until benchmarked
    >-- proxy_buffers 4 256k;      # disabled until benchmarked
    >-- proxy_busy_buffers_size 256k; # disabled until benchmarked
    >-- reset_timedout_connection on; # disabled until benchmarked
    > end
    
        client_max_body_size {{ .Values.global.CLIENT_MAX_BODY_SIZE }};
        proxy_ssl_server_name on;
        underscores_in_headers on;
    
        lua_package_path '{{ .Values.global.LUA_PACKAGE_PATH }};;';
        lua_package_cpath '{{ .Values.global.LUA_PACKAGE_CPATH }};;';
        lua_socket_pool_size {{ .Values.global.LUA_SOCKET_POOL_SIZE }};
        lua_max_running_timers 4096;
        lua_max_pending_timers 16384;
        lua_shared_dict kong                5m;
        lua_shared_dict kong_db_cache       {{ .Values.global.MEM_CACHE_SIZE }};
    > if database == "off" then
        lua_shared_dict kong_db_cache_2     {{ .Values.global.MEM_CACHE_SIZE }};
    > end
        lua_shared_dict kong_db_cache_miss   12m;
    > if database == "off" then
        lua_shared_dict kong_db_cache_miss_2 12m;
    > end
        lua_shared_dict kong_locks          8m;
        lua_shared_dict kong_process_events 5m;
        lua_shared_dict kong_cluster_events 5m;
        lua_shared_dict kong_healthchecks   5m;
        lua_shared_dict kong_rate_limiting_counters 12m;
        lua_socket_log_errors off;
    > if lua_ssl_trusted_certificate then
        lua_ssl_trusted_certificate '{{ .Values.global.LUA_SSL_TRUSTED_CERTIFICATE }}';
    > end
        lua_ssl_verify_depth {{ .Values.global.LUA_SSL_VERIFY_DEPTH }};
    
        lua_shared_dict kong_mock_upstream_loggers 10m;
    
    # injected nginx_http_* directives
    > for _, el in ipairs(nginx_http_directives) do
        $(el.name) $(el.value);
    > end
    
        init_by_lua_block {
            Kong = require 'kong'
            Kong.init()
        }
    
        init_worker_by_lua_block {
            Kong.init_worker()
        }
    
    > if #proxy_listeners > 0 then
        upstream kong_upstream {
            server 0.0.0.1;
            balancer_by_lua_block {
                Kong.balancer()
            }
    > if upstream_keepalive > 0 then
            keepalive {{ .Values.global.UPSTREAM_KEEPALIVE }};
    > end
        }
    
        server {
            server_name kong;
    > for i = 1, #proxy_listeners do
            listen $(proxy_listeners[i].listener);
    > end
            error_page 400 404 408 411 412 413 414 417 494 /kong_error_handler;
            error_page 500 502 503 504 /kong_error_handler;
    
            access_log logs/access.log timed_combined;
    
            client_body_buffer_size {{ .Values.global.CLIENT_BODY_BUFFER_SIZE }};
    
    > if proxy_ssl_enabled then
            ssl_certificate {{ .Values.global.SSL_CERT }};
            ssl_certificate_key {{ .Values.global.SSL_CERT_KEY }};
            ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
            ssl_certificate_by_lua_block {
                Kong.ssl_certificate()
            }
    > end
    
            real_ip_header     {{ .Values.global.REAL_IP_HEADER }};
            real_ip_recursive  {{ .Values.global.REAL_IP_RECURSIVE }};
    > for i = 1, #trusted_ips do
            set_real_ip_from   $(trusted_ips[i]);
    > end
    
            # injected nginx_proxy_* directives
    > for _, el in ipairs(nginx_proxy_directives) do
            $(el.name) $(el.value);
    > end
    
            location / {
                default_type '';
    
                set $ctx_ref                     '';
                set $upstream_te                 '';
                set $upstream_host               '';
                set $upstream_upgrade            '';
                set $upstream_connection         '';
                set $upstream_scheme             '';
                set $upstream_uri                '';
                set $upstream_x_forwarded_for    '';
                set $upstream_x_forwarded_proto  '';
                set $upstream_x_forwarded_host   '';
                set $upstream_x_forwarded_port   '';
    
                rewrite_by_lua_block {
                    Kong.rewrite()
                }
    
                access_by_lua_block {
                    Kong.access()
                }
    
                proxy_http_version 1.1;
                proxy_set_header   Host              $upstream_host;
                proxy_set_header   Upgrade           $upstream_upgrade;
                proxy_set_header   Connection        $upstream_connection;
                proxy_set_header   X-Forwarded-For   $upstream_x_forwarded_for;
                proxy_set_header   X-Forwarded-Proto $upstream_x_forwarded_proto;
                proxy_set_header   X-Forwarded-Host  $upstream_x_forwarded_host;
                proxy_set_header   X-Forwarded-Port  $upstream_x_forwarded_port;
                proxy_set_header   X-Real-IP         $remote_addr;
                proxy_pass_header  Server;
                proxy_pass_header  Date;
                proxy_ssl_name     $upstream_host;
                proxy_pass         $upstream_scheme://kong_upstream$upstream_uri;
    
                header_filter_by_lua_block {
                    Kong.header_filter()
                }
    
                body_filter_by_lua_block {
                    Kong.body_filter()
                }
    
                log_by_lua_block {
                    Kong.log()
                }
            }
    
            location = /kong_error_handler {
                internal;
                uninitialized_variable_warn off;
    
                content_by_lua_block {
                    Kong.handle_error()
                }
    
                header_filter_by_lua_block {
                    Kong.header_filter()
                }
    
                body_filter_by_lua_block {
                    Kong.body_filter()
                }
    
                log_by_lua_block {
                    Kong.log()
                }
            }
        }
    > end
    
    > if #admin_listeners > 0 then
        server {
            server_name kong_admin;
    > for i = 1, #admin_listeners do
            listen $(admin_listeners[i].listener);
    > end
    
            access_log logs/admin_access.log;
    
            client_max_body_size 10m;
            client_body_buffer_size 10m;
    
    > if admin_ssl_enabled then
            ssl_certificate {{ .Values.global.ADMIN_SSL_CERT }};
            ssl_certificate_key {{ .Values.global.ADMIN_SSL_CERT_KEY }};
            ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
    > end
    
            # injected nginx_admin_* directives
    > for _, el in ipairs(nginx_admin_directives) do
            $(el.name) $(el.value);
    > end
    
            location / {
                default_type application/json;
                content_by_lua_block {
                    Kong.serve_admin_api()
                }
            }
    
            location /nginx_status {
                internal;
                access_log off;
                stub_status;
            }
    
            location /robots.txt {
                return 200 'User-agent: *\nDisallow: /';
            }
        }
    > end
    
    > end
    }
