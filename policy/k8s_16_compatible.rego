package main

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "extensions/v1beta1"
  input.kind = "NetworkPolicy"
  msg = sprintf("%v/%v: extensions/v1beta1 for NetworkPolicy is deprecated use networking.k8s.io/v1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "extensions/v1beta1"
  input.kind = "PodSecurityPolicy"
  msg = sprintf("%v/%v: extensions/v1beta1 for PodSecurityPolicy is deprecated use policy/v1beta1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "extensions/v1beta1"
  input.kind = "DaemonSet"
  msg = sprintf("%v/%v: extensions/v1beta1 for DaemonSet is deprecated use apps/v1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "apps/v1beta2"
  input.kind = "DaemonSet"
  msg = sprintf("%v/%v: apps/v1beta2 for DaemonSet is deprecated use apps/v1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "extensions/v1beta1"
  input.kind = "Deployment"
  msg = sprintf("%v/%v: extensions/v1beta1 for Deployment is deprecated use apps/v1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "apps/v1beta2"
  input.kind = "Deployment"
  msg = sprintf("%v/%v: apps/v1beta2 for Deployment is deprecated use apps/v1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "extensions/v1beta1"
  input.kind = "StatefulSet"
  msg = sprintf("%v/%v: extensions/v1beta1 for Deployment is deprecated use apps/v1 instead.", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.apiVersion = "apps/v1beta2"
  input.kind = "StatefulSet"
  msg = sprintf("%v/%v: apps/v1beta2 for Deployment is deprecated use apps/v1 instead.", [kind, name])
}