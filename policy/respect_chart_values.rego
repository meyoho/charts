package main

expected_memory := "1Mi"
expected_cpu := "1m"

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.kind = "Deployment"
  container := input.spec.template.spec.containers[_]
  container.resources.requests.memory != expected_memory
  msg = sprintf("%v/%v: container %v's resources.request.memory is not same as chart vaule", [kind, name, container.name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.kind = "Deployment"
  container := input.spec.template.spec.containers[_]
  container.resources.requests.cpu != expected_cpu
  msg = sprintf("%v/%v: container %v's resources.request.cpu is not same as chart vaule", [kind, name, container.name])
}