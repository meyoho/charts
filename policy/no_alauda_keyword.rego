package main

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.metadata.labels[i]
  contains(i, "alauda")
  msg = sprintf("%v/%v: Labels '%v' must not contain alauda keyword", [kind, name, i])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name

  input.metadata.annotations[i]
  contains(i, "alauda")
  msg = sprintf("%v/%v: Annotations '%v' must not contain alauda keyword", [kind, name, i])
}
