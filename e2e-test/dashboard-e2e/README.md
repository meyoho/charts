# Dashboard Test Chart

## 参数介绍

* --set global.registry.address=10.0.0.2  设置镜像所在的registry地址
* --set global.image.link.tag=v1.3.0      设置dashboard测试镜像的版本号，一般使用默认即可
* --set report=/tmp/link                  设置测试结果挂载路径，测试完成后可以在该路径下查看测试报告
* --set diablo.base_url=https://alaudak8s-prod.alauda.cn/console/  设置dashboard测试访问的地址
