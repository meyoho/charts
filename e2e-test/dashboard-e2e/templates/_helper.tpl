{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "dashboard.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "link.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.link.repository -}}
{{- $tag := .Values.global.images.link.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "helloworld.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.helloworld.repository -}}
{{- $tag := .Values.global.images.helloworld.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}
