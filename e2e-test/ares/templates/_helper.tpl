{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "ares.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "ares.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.ares.repository -}}
{{- $tag := .Values.global.images.ares.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "httpd.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.httpd.repository -}}
{{- $tag := .Values.global.images.httpd.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}
