# devops add data Chart

## 1. 执行添加数据

执行命令: make install-tools
执行命令: make install-pipeline

#### 必选参数

-   REGISTRY=index.alauda.cn   镜像仓库地址，登陆 master 节点，docker images 可查询到镜像仓库
-   PROJECT_NAME=e2edata-project      创建的项目名称
-   JENKINS_NAME=jenkins      需要集成jenkins名称
-   JENKINS_HOST=http://62.234.104.184:32001  集成的jenkins地址
-   JENKINS_TOKEN=MTE4MDI3MmY3MDdmZTdjODJiN2M5MjExOTNmZDhkODZlOAo=  jenkins token
-   HARBOR_HTTP=https://   需要使用到的harbor集成时的http协议
-   HARBOR_HOST=harbor.devsparrow.host  集成Harbor的地址
-   HARBOR_TOKEN=SGFyYm9yMTIzNDUK       Harbor的token
-   GITLAB_HOST=http://62.234.104.184:31101  集成Gitlab的地址
-   GITLAB_TOKEN=Zlotc3NReDZLdV9YUnFVeFk3QngK  Gitlab的token

#### 可选参数
