{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "stability.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "stability.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.stability.repository -}}
{{- $tag := .Values.global.images.stability.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "httpd.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.httpd.repository -}}
{{- $tag := .Values.global.images.httpd.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "nomal.image" -}}
{{- $registryAddress := .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.image.repository -}}
{{- $tag := .Values.global.images.image.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "stress.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.stress_image.repository -}}
{{- $tag := .Values.global.images.stress_image.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "stability.configmap.name" -}}
{{- $stability_name :=  .Values.nameOverride -}}
{{- printf "%s-%s" $stability_name "cm-env" -}}
{{- end -}}