# ASM Demo Chart

ASM Demo Chart 主要用于 ASM 安装成功后， 服务拓扑图的验证

## 1. 如何安装

执行命令:
helm install ./ --name asm-demo --namespace alauda-system \
 --set global.registry.address=\${REGISTRY} \
 --set projectname=asm-demo

## 2. 如何卸载

执行命令: helm del --purge asm-demo
