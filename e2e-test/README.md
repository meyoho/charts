# e2e-test

## overview

e2e-test下包含了对于alauda-dashboard,alauda-appcore,alauda-devops-apiserver相关测试的 `chart`, 目标是能够在私有发版时使用chart管理并进行测试, 目前包含的 `chart` 有：

* [appcore]
* [dashboard]
* [devops]
* [portal]

详细信息, 看各自的 `README.md` 文档.  部分命令可以参考各自目录下的 `Makefile` 文件.