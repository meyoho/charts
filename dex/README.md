# 部署 Dex 组件说明

### 1.生成证书
运行以下脚本（需要先修改相关的域名或IP等参数），会在当前目录下生成 ssl 文件夹，里面有相关的证书文件。
```
#!/bin/bash
mkdir -p ssl
cat << EOF > ssl/req.cnf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
# 此处填写需要在证书中添加的域名白名单，格式为 DNS.1、DNS.2、DNS.3，依次类推。就是访问平台的域名地址
DNS.1 = alaudak8s-staging.alauda.cn
# 此处填写需要在证书中添加的IP白名单，格式为 IP.1、IP.2、IP.3，依次类推。就是上一个域名解析出来的 ip
IP.1 = 1.2.3.4
EOF
openssl genrsa -out ssl/ca-key.pem 2048
openssl req -x509 -new -nodes -key ssl/ca-key.pem -days 8000 -out ssl/ca.pem -subj "/CN=kube-ca"
openssl genrsa -out ssl/key.pem 2048
openssl req -new -key ssl/key.pem -out ssl/csr.pem -subj "/CN=kube-ca" -config ssl/req.cnf
openssl x509 -req -in ssl/csr.pem -CA ssl/ca.pem -CAkey ssl/ca-key.pem -CAcreateserial -out ssl/cert.pem -days 8000 -extensions v3_req -extfile ssl/req.cnf
```

### 2.安装dex
chart 暂存在 Bitbucket 的仓库中。https://bitbucket.org/mathildetech/charts
如果用户部署 AKE 的时候有设置 host 相关参数，则需要传入这些参数覆盖 chart 中的默认参数：

```
helm install \
--name alauda-dex \
--set dex.host=<dex_host> \
--set consoleacp.host=<consoleacp_host> \
--set consoledevops.host=<consoledevops_host> \
--set consoleasm.host=<consoleasm_host> \
--set consoleplatform.host=<consoleplatform_host> \
--set secret.tlsCrt=<tlsCrt> \
--set secret.tlsKey=<tlsKey> \
./charts/dex
```

helm install参数说明:
```
<dex_host>              dex访问地址. 如https://xxx.com/
<consoleacp_host>       acp访问地址. 如https://xxx.com/console-acp
<consoledevops_host>    devops访问地址. 如https://xxx.com/console-devops
<consoleasm_host>       asm访问地址. 如https://xxx.com/console-asm
<consoleplatform_host>  platform访问地址. 如https://xxx.com/console-platform
<tlsCrt>                ssl证书 需要base64转码。对应文件cert.pem
<tlsKey>                ssl key 需要base64转码。对应文件key.pem
```
#### 初始化非默认管理员账号，安装参数（选填）
(备注: 小于等于`2.8`版本采用以下配置方式)
```
--set configmap.default_admin=<email>
```
初始化非默认管理员账号，按照参数(说明)
```
<email>                 邮箱,如admin@cpaas.io
```

(备注：大于等于`2.9`版本采用以下配置方式)
```
--set default_admin.email=<email>
--set default_admin.name=<name>
--set default_admin.hash=<hash>
--set default_admin.account=<account>
--set default_admin.username=<username>
--set default_admin.isAdmin=<isAdmin>
```
初始化非默认管理员账号，按照参数(说明)
```
<email>                邮箱,如admin@cpaas.io
<name>                 邮箱小写后的md5值,如8afe307d054f1e96ce2e3d10aa374162
<hash>                 密码,进过bcrypt算法加密，再通过base64加密
<account>              用户名,用户可以使用用户名进行登录
<username>             显示名称,
<isAdmin>              表明管理员账户，默认为true
```

注：上面参数中的各个 host 不要带 http:// 或 https:// 等前缀，使用域名或IP即可。一般来说各组件的 host 地址都跟 dex 组件的 host 地址一致。

### 3.K8s apiserver 增加参数


- --oidc-issuer-url
    - OIDC 服务提供商（此处为dex）的URL，必须为 https 协议。如：https://accounts.google.com 或 https://dex.example.com:61000
    - 该值固定为 https://<dex的host地址>/dex
    
- --oidc-client-id
    - OIDC 客户端的唯一标识（ID）
    - 该值固定为 alauda-auth

- --oidc-ca-file
    - OIDC 证书路径
    - 证书使用第一步生成的证书 cert.pem  ，拷贝到路径下：/etc/kubernetes/pki/  ，key.pem 也 cp 到这个目录
- --oidc-username-claim
    - 从 JWT 的声明字段中选取哪个字段作为用户名
    - 该值固定为 email
- --oidc-groups-claim
    - 从 JWT 的声明字段中选取哪个字段作为分组名
    - 该值固定为 groups


例子
```
- --oidc-issuer-url=https://10.0.128.75/dex
- --oidc-client-id=alauda-auth
- --oidc-ca-file=/etc/kubernetes/pki/cert.pem
- --oidc-username-claim=email
- --oidc-groups-claim=groups
```
说明: master及业务集群master的apiserver都需配置oidc的，不然使用用户token会报401未授权.

### 4.Auth 开启后的默认管理员账号
```
账号：admin@alauda.io
密码：password
```

## 5.(`大于等于2.9版本`)Dex升级需要手动操作的事项
1. 停掉auth-controller2，操作方式为将auth-controller2的scale到0
    ```
    kubectl scale deployment -n <system-namespace>  auth-controller2 --replicas=0
    ```
    system-namespace指系统命名空间，比如alauda-system,cpaas-system等等
2. 删除dex-configmap
    ```
    kubectl delete cm -n <system-namespace> dex-configmap
    ```
    system-namespace指系统命名空间，比如alauda-system,cpaas-system等等
3. 升级
