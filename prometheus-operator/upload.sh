#!/usr/bin/env bash

helm dep update kube-prometheus

CHARTS="alertmanager exporter-coredns exporter-kube-controller-manager exporter-kube-dns exporter-kube-etcd exporter-kube-scheduler exporter-kube-state exporter-kubelets exporter-kubernetes exporter-node grafana kube-prometheus prometheus prometheus-operator"

for chart in $CHARTS
do
    helm package $chart
done

for tgz in `ls | grep tgz`
do
    curl -v --fail -F "chart=@$tgz" http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-kubernetes-charts.alauda.cn/api/charts
done