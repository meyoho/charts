
There are a series of resources about logging, monitoring and metering in chart alauda-aiops, including lanaya, morgans, mars, courier and some related crd resources.

Morgans/courier/lanaya are used for logging function, while morgans/mars/courier are used for monitoring and metering. Yes, morgans/courier is used for multi functions.
So morgans and courier resources is located in base dir, lanaya in logging, mars in monitoring, meter crd in metering, notification crd in notification, located by function of resources.

Two options(deployLog/deployMonitor) to decide which function to enable, deployLog enable resources in base and logging dirs, while deployMonitor enable resources in base, metering, monitoring, notification dir. Both are true by default.
