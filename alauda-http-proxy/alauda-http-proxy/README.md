# HTTP proxy Helm Chart

## Configuration

The following tables list the configurable parameters of the Jenkins chart and their default values.

### Jenkins Master
| Parameter             | Description               | Default            |
| ----------------------| ------------------------- | ------------------ |
| `proxy.nexusHost`     | The nexus server host     | `nexus-b.alauda.cn`|
| `proxy.nexusUsername` | Nexus server username     | `admin`            |
| `proxy.nexusPassword` | Nexus server password     | `admin`            |
| `proxy.proxyIgnore`   | Ignore parts of host which does not pass the proxy  | `security.ubuntu.com`  |
