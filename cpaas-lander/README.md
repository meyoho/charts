# Welcome to [cpaas-lander](https://bitbucket.org/mathildetech/alauda-lander) 👋

> The application is a Openshift token proxy server.

## 部署说明
- 安装包默认namespace为`cpaas-lander`, 请手动创建ns；
- 该组件默认开放nodeport端口，acp通过访问nodeport作为代理方式接入openshift；
- openshift集群集成的k8s版本过低，需要给组件`cluster-registry-manager`增加环境变量`SUPPORTED_VERSIONS`指定k8s版本；
- dex接入的第三方账号，在openshift里再接入一次；

## Charts最简安装

```sh
helm install . \
    --set lander.oidc_issuer_url=https://129.28.182.197/dex \
    --set lander.openshift_server_address=192.168.99.100:8443 \
    --set nodePort.port=32008
```

## charts主要参数说明

```sh
oidc_issuer_url             dex issuer address, eg: https://129.28.182.197/dex
oidc_client_id              dex client id, default: alauda-auth
openshift_server_proto      openshift apiserver schema (http or https)
openshift_server_address    openshift apiserver address, eg: 10.0.128.51:8443

nodePort:
  enabled: true             是否开发nodeport，默认开发
  port: 32008               端口，默认32008
```

## openshift第三方账号接入
注：dex里配置的idp conn_id必须和openshift里配置的idp name保持一致
openldap配置例子
```$xslt
  - name: "ldapprovider"
    challenge: true
    login: true
    mappingMethod: claim
    provider:
      apiVersion: v1
      kind: LDAPPasswordIdentityProvider
      attributes:
        email:
        - mail
        id:
        - mail
        preferredUsername:
        - mail
      bindDN: "cn=admin,dc=example,dc=org"
      bindPassword: "admin"
      insecure: true
      url: "ldap://10.0.128.51:32002/dc=example,dc=org?mail"
```

keycloak oidc配置例子
```$xslt
  - name: keycloak
    challenge: true
    login: true
    mappingMethod: claim
    provider:
      apiVersion: v1
      kind: OpenIDIdentityProvider
      clientID: keycloak
      clientSecret: 0dc76efe-b033-438c-8b9b-7aeb17fac921
      ca: cert.pem 
      claims:
        id: 
        - user_name
        preferredUsername: 
        - user_name
        name: 
        - user_name
        email: 
        - email
      urls:
        authorize: https://10.0.128.51:8848/auth/realms/master/protocol/openid-connect/auth
        token: https://10.0.128.51:8848/auth/realms/master/protocol/openid-connect/token
        userInfo: https://10.0.128.51:8848/auth/realms/master/protocol/openid-connect/userinfo
```
