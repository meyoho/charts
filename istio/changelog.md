## 2019-03-25
添加gatewaynodeaffinity支持HostPort部署
ingressGateway只保留80和443端口
修改resource默认资源限制
修改grafana的支持通过ingress的subpath访问

## 2019-03-26
添加Pilot环境变量PILOT_DIRECT_EDS: "0"
Pilot镜像1.0.5为ASM定制版本，李旭东fix bug: https://github.com/istio/istio/pull/12418

## 2019-03-27
修改Configmap中zipkinAddress的变量为global.zipkinAddress,默认 alauda-zipkin:9411

## 2019-04-09
global.zipkinAddress 变量默认值改为 ns-zipkin:9411

## 2019-04-18
去掉ingressgateway的自动扩缩容

## 2019-04-04
升级到istio 1.1.3

## 2019-05-07
因为istio升级到1.1.3, sidecar的cluster字段带上了namespace，jaeger那边可以借助这个进行namespace隔离，所以global.zipkinAddress默认值改为 zipkin.istio-system:9411

## 2019-11-13
反亲和需求，给istio组件podAntiAffinityLabelSelector设置了反亲和value

## 2019-11-20
修改ingressgateway的service为ClusterIP,流水线上helm --wait，对loadbalance这样的pending状态不支持

## 2019-12-6
升级istio到1.3
