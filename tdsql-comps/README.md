###部署参数说明
Values.chitu.replicas: 赤兔副本数量
Values.chitu.db: 监控库Credential信息，包含ip、port、用户名和密码，默认库的名字tdsqlpcloud一般不需要改变
Values.chitu.hostlist: 赤兔所有监控库的ip:port列表，主要用于赤兔的故障转移
Values.oss.replicas: OSS副本数量
Values.oss.zk_num/zk_ip_list/zk_rootdir：ZK相关的设置，OSS依赖于ZK
Values.oss.wait_for_change_localip: OSS所在服务的地址，前向兼容于之前的IP设置
Values.oss.wait_for_change_clouddbaip：赤兔所在服务的地址，前向兼容于之前的IP设置
Values.oss.metadb: 同样也是监控库Credential信息，等同于Values.chitu.db。之所以分开设置，主要是为了解耦合
