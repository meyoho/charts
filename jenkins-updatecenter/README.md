# Helm Chart for Private Jenkins UpdateCenter

## 介绍

Private Jenkins UpdateCenter 是[Jenkins UpdateCenter](https://updates.jenkins-ci.org/) 的私有化方案。 可以用来在私有环境中，提供 jenkins updatecenter的功能 

此 chart 定义了 Jenkins UpddateCenter 组件. 这包括:

- updatecenter: 主站点

目前，updatecenter 还需要依赖一个后端的Nexus3 的存储。

## 环境要求

- Kubernetes 1.10 及以上版本、Beta APIs
- Kubernetes Ingress Controller
- Helm 2.8.0 及以上版本

## 安装 Chart

要安装版本名为 "my-release" 的 chart, 请运行:

```
$ helm install --name my-release \
   --set nexus3.address=http://your-nexus3/you-maven-repository/ .
```

> **Tip**: 使用 `helm list` 列出所有版本

## 卸载 Chart

卸载/删除 `my-release`:

```
$ helm delete my-release
```

该命令将删除与 `chart` 关联的所有 `Kubernetes` 资源并删除该版本. 

## 配置

可以到 [values.yaml](values.yaml) 中查看所有配置的默认值, 使用 `helm install` 的 `--set key = value [, key = value]` 参数设置每个参数.

或者, 可以在安装 `chart` 时提供指定参数值的 `YAML` 文件. 例如:

```
$ helm install --name my-release -f values.yaml .
```

> **Tip**: 你可以使用默认配置文件 [values.yaml](values.yaml)

### 镜像地址

在一个私有的网络环境，连接到外网会受限制，所以需要使用本地的镜像仓库。当指定镜像仓库地址时，如果指定一个 IP 地址为 `10.0.0.2`，并可以访问的镜像仓库地址，运行命令：

```
--set global.registry.address=10.0.0.2
```

## 访问方式

### 通过 ip 访问

部署 jenkins upatecenter 时候，需要确定 updatecenter 的访问方式, 如果没有可用的域名，也可以通过 `<nodeIP>:<nodePort>` 的方式来访问，示例如下：

```
helm install . --name jenkins-updatecenter \
--set updatecenter.serviceType=NodePort \
--set updatecenter.nodePort=32606 \
--set nexus3.address=http://your-nexus3/you-maven-repository/
```

nodePort 的值应该在 `30000` 到 `32767` 中间取值，不要与集群其它服务端口冲突。

### 通过域名访问

```
helm install . --name jenkins-updatecenter \
--set ingress.enabled=true \
--set ingress.host=updates.com \
--set nexus3.address=http://your-nexus3/you-maven-repository/
```

## 配置项
| Parameter                                   | Description                               | Default                                    |
| ------------------------------------------  | ----------------------------------------  | -------------------------------------------|
| `global.registry.address`                   | 目标镜像所在的 registry                      | `index.aladua.cn`                         |
| `global.images.updatecenter.repository`     | 目标镜像的repository名称                     |   `devops/jenkins-updatecenter`        |
| `global.images.updatecenter.tag`     | 目标镜像的tag                               |                                           |
| `global.namespace`                          | 部署的目标 namespace                        |  `alauda-system`                          |
| `ingress.enabled`                           | Flag for enabling ingress                 | false                                      |
| `ingress.annotations`                       | Ingress additional annotations                 | `{}`                                       |
| `ingress.host`                              | Hostname to your jenkins updatecenter installation   | `your-domain.com`                   |
| `ingress.tls.enabled`                       | 启用 http                                 | `false`                                       |
| `registrySecret.name`                       | 获取镜像所需要的 凭据                        |  `devops`                                         |
| `updatecenter.cron`               | 定期生成静态站点的时间间隔    | `*/10 * * * *`   |
| `updatecenter.nexus3.address`               | 后端的 nexus3 的地址，需要指定到具体的repo     | `http://your-nexus3-address/your-maven-repo-name/`   |
| `updatecenter.nexus3.repository`             | 后端的 nexus3的具体的repo的名字     | `maven-snapshots`   |
| `updatecenter.caps`               | 用来生成指定LTS版本的updatecenter plugin 目录， 例如 2.190， 多个时使用空格隔开。 查看目前jenkins支持的LTS https://updates.jenkins.io/   |  `2.190`   |
| `updatecenter.args`               | 生成静态站点时的参数， 和 `https://github.com/ikedam/backend-update-center2` 文中描述一致   |  `-includeSnapshots -enableProxy`   |
| `updatecenter.servicePort`               | serviceType=ClusterIP时的ServicePort     | `8080`   |
| `updatecenter.serviceType`               | ServiceType    | `ClusterIP`   |
| `updatecenter.nodePort`               | serviceType=NodePort时的 NodePort   | `32606`   |
| `updatecenter.resources.requests.memory`               | 请求的内存大小,  当开启 proxy 时，推荐2G     | `200Mi`   |
| `updatecenter.resources.requests.cpu`               | 请求的cpu大小     | `50m`   |
| `updatecenter.resources.limits.memory`               | 限制的内存大小    | `800Mi`   |
| `updatecenter.resources.limits.cpu`               | 限制的 cpu大小    | `200m`   |
| `Persistence.Enabled`       | Enable the use of a Jenkins PVC | `true`          |
| `Persistence.ExistingClaim` | Provide the name of a PVC       | `nil`           |
| `Persistence.AccessMode`    | The PVC access mode             | `ReadWriteOnce` |
| `Persistence.Size`          | The size of the PVC             | `8Gi`           |
| `Persistence.volumes`       | Additional volumes              | `nil`           |
| `Persistence.mounts`        | Additional mounts               | `nil`           |


## Persistence

The JenkinsUpdateCenter image stores all static files under `/app/www` path of the container. A dynamically managed Persistent Volume
Claim is used to keep the data across deployments, by default. This is known to work in GCE, AWS, and minikube. Alternatively,
a previously configured Persistent Volume Claim can be used.

It is possible to mount several volumes using `Persistence.volumes` and `Persistence.mounts` parameters.

### 推荐配置

As a minimum for running in production, the following settings are advised:

```yaml
global:
  namespace: alauda-system
registrySecret:
  name: devops
updatecenter:
  args: "-includeSnapshots -enableProxy"
  cron: "*/10 * * * *"
  caps: "2.190"
  nexus3:
    address: "http://your-nexus3-address/your-maven-repo-name/"
    repository: maven-snapshots
  resources:
    # 不设置 -enableProxy 时
    # requests:
    #   memory: 500Mi
    #   cpu: 300m
    # limits:
    #   memory: 200Mi
    #   cpu: 100m
    # 设置 -enableProxy 时
    requests:
      memory: 1G
      cpu: 200m
    limits:
      memory: 3G
      cpu: 500m
ingress:
  enabled: true
  host: update-jenkins.your.domain.com
  annotations:
    certmanager.k8s.io/acme-challenge-type: http01
    certmanager.k8s.io/cluster-issuer: letsencrypt-prod
  tls:
  - secretName: update-jenkins.your.domain.com
    hosts:
      - update-jenkins.your.domain.com
```

## Design
- updatecenter 中使用Caddy 提供一个 静态的http server
- 有一个定时任务，定期访问 nexus3， 生成静态站点。