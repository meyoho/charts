package main

deny[msg] {
  kind := input.kind
  name := input.metadata.name
  check_kinds := {"Deployment", "DaemonSet", "StatefulSet"}

  check_kinds[_] = kind
  count({label | input.spec.template.metadata.labels[label]; label == "service_name"}) == 0
  msg = sprintf("%v/%v: must contain service_name label", [kind, name])
}

deny[msg] {
  kind := input.kind
  name := input.metadata.name
  check_kinds := {"Deployment", "DaemonSet", "StatefulSet"}

  check_kinds[_] = kind
  count({label | input.spec.template.metadata.labels[label]; contains(label, "/product")}) == 0
  msg = sprintf("%v/%v: must contain product label", [kind, name])
}