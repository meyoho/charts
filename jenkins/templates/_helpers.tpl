{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "jenkins.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "jenkins.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "jenkins.version" -}}
  {{- $split := splitList "-" .Values.global.images.jenkins.tag }}
  {{- printf "%s" (index $split 0 ) -}}
{{- end -}}

{{- define "jenkins.auth.enable" -}}
  {{- if or .Values.Master.AuthorizationStrategy.ProjectMatrix .Values.oidc.enable -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.clientId" -}}
  {{- if ne .Values.oidc.clientID "" -}}
    {{- .Values.oidc.clientID -}}
  {{- else if ne .Values.dex.clientId "" -}}
    {{- .Values.dex.clientId -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.clientsecret" -}}
  {{- if ne .Values.oidc.clientSecret "" -}}
    {{- .Values.oidc.clientSecret -}}
  {{- else if ne .Values.dex.clientSecret "" -}}
    {{- .Values.dex.clientSecret -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.issuer" -}}
  {{- if ne .Values.oidc.issuer "" -}}
    {{- .Values.oidc.issuer -}}
  {{- else if ne .Values.dex.issuer "" -}}
    {{- .Values.dex.issuer -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.oidcVerifyCert" -}}
  {{- if .Values.dex.disableSslVerification -}}
    {{- printf "true" -}}
  {{- else if .Values.oidc.verifyCert -}}
    {{- printf "false" -}}
  {{- else if and (not .Values.oidc.verifyCert) .Values.oidc.enable -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.emailFieldName" -}}
  {{- if ne .Values.oidc.jenkins.emailFieldName "" -}}
    {{- .Values.oidc.jenkins.emailFieldName -}}
  {{- else if ne .Values.dex.emailFieldName "" -}}
    {{- .Values.dex.emailFieldName -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.escapeHatchEnabled" -}}
  {{- if .Values.oidc.jenkins.escapeHatchEnabled -}}
    {{- printf "true" -}}
  {{- else if .Values.dex.escapeHatchEnabled -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.escapeHatchGroup" -}}
  {{- if ne .Values.oidc.jenkins.escapeHatchGroup "" -}}
    {{- .Values.oidc.jenkins.escapeHatchGroup -}}
  {{- else if ne .Values.dex.escapeHatchGroup "" -}}
    {{- .Values.dex.escapeHatchGroup -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.escapeHatchSecret" -}}
  {{- if ne .Values.oidc.jenkins.escapeHatchSecret "" -}}
    {{- .Values.oidc.jenkins.escapeHatchSecret -}}
  {{- else if ne .Values.dex.escapeHatchSecret "" -}}
    {{- .Values.dex.escapeHatchSecret -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.escapeHatchUsername" -}}
  {{- if ne .Values.oidc.jenkins.escapeHatchUsername "" -}}
    {{- .Values.oidc.jenkins.escapeHatchUsername -}}
  {{- else if ne .Values.dex.escapeHatchUsername "" -}}
    {{- .Values.dex.escapeHatchUsername -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.fullNameFieldName" -}}
  {{- if ne .Values.oidc.jenkins.fullNameFieldName "" -}}
    {{- .Values.oidc.jenkins.fullNameFieldName -}}
  {{- else if ne .Values.dex.fullNameFieldName "" -}}
    {{- .Values.dex.fullNameFieldName -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.groupsFieldName" -}}
  {{- if ne .Values.oidc.jenkins.groupsFieldName "" -}}
    {{- .Values.oidc.jenkins.groupsFieldName -}}
  {{- else if ne .Values.dex.groupsFieldName "" -}}
    {{- .Values.dex.groupsFieldName -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.logoutFromOpenidProvider" -}}
  {{- if .Values.oidc.jenkins.logoutFromOpenidProvider -}}
    {{- printf "true" -}}
  {{- else if .Values.dex.logoutFromOpenidProvider -}}
    {{- printf "true" -}}
  {{- else -}}
    {{- printf "false" -}}
  {{- end -}}
{{- end -}}

{{- define "jenkins.auth.userNameField" -}}
  {{- if ne .Values.oidc.jenkins.userNameField "" -}}
    {{- .Values.oidc.jenkins.userNameField -}}
  {{- else if ne .Values.dex.userNameField "" -}}
    {{- .Values.dex.userNameField -}}
  {{- end -}}
{{- end -}}
