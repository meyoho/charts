#!/bin/bash

curVersion="v1.6.0"
images=(
    # harbor
    "goharbor/harbor-portal:dev alaudak8s/harbor-portal:${curVersion}"
    "goharbor/harbor-core:dev alaudak8s/harbor-core:${curVersion}"
    "goharbor/harbor-adminserver:dev alaudak8s/harbor-adminserver:${curVersion}"
    "goharbor/harbor-jobservice:dev alaudak8s/harbor-jobservice:${curVersion}"
    "goharbor/harbor-db:dev alaudak8s/harbor-db:${curVersion}"
    "goharbor/registry-photon:dev alaudak8s/harbor-registry-registry:${curVersion}"
    "goharbor/harbor-registryctl:dev alaudak8s/harbor-registry-controller:${curVersion}"
    "goharbor/chartmuseum-photon:dev alaudak8s/harbor-chartmuseum:${curVersion}"
    "goharbor/clair-photon:dev alaudak8s/harbor-clair:${curVersion}"
    "goharbor/notary-server-photon:dev alaudak8s/harbor-notary-server:${curVersion}"
    "goharbor/notary-signer-photon:dev alaudak8s/harbor-notary-signer:${curVersion}"
    # gitlab
    "sameersbn/gitlab:11.4.0 alaudak8s/gitlab-ce:11.4.0"
    "sameersbn/postgresql:10 alaudak8s/postgresql:10"
    "sameersbn/redis:4.0.9-1 alaudak8s/redis:4.0.9-1"
    # sonarqube
    "postgres:9.6.2 alaudak8s/postgres:9.6.2"
    "sonarqube:7.9.1-community alaudak8s/sonarqube:7.9.1-community"
    "busybox:1.31 alaudak8s/busybox:1.31"
    "joosthofman/wget:1.0 alaudak8s/joosthofman-wget:1.0"
    "dduportal/bats:0.4.0 alaudak8s/dduportal-bats:0.4.0"
    "wrouesnel/postgres_exporter:v0.1.1 alaudak8s/wrouesnel-postgres_exporter:v0.1.1"
    # others
    "gomods/athens:v0.7.0 alaudak8s/athens:v0.7.0"
    "alpine:3.9 alaudak8s/alpine:3.9"
    "jaegertracing/all-in-one:latest alaudak8s/jaegertracing/all-in-one:latest"
)

for i in "${images[@]}" ; do
    b=($i)
    docker pull ${b[0]}
    docker tag ${b[0]} "index.alauda.cn/${b[1]}"
    docker push "index.alauda.cn/${b[1]}"
done