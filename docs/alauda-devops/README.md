# AlaudaDevOps Chart Document

Alauda DevOps Chart 负责完成Alauda DevOps 产品的安装部署升级.  
按照目前我们对 chart的规划， AlaudaDevOps 的组件被划分到 几个Chart中， 

- alauda-base: 包含 devops-apiserver 和  devops-controller 两个组件
- alauda-devops: 主要包含 diablo frontent, diablo backend, console 等组件
- 其他devops工具的chart：jenkins, gitlab, harbor

## alauda-base

包含的deployment 如下
- devops-apiserver
  定义 devops的相关资源，处理 devops 资源的 创建，删除， 更新，获取等操作。
  例如，工具的集成绑定，流水线的创建等等。

- devops-controller
  根据devops 资源的定义，对资源的状态进行同步。
  例如，工具的可用状态的轮询检测，代码仓库的轮询同步等等。
  启动时，会使用 devopsPipelineTemplates 容器， 作为 init container， 将 流水线模板导入到集群中。

## alauda-devops

包含的deployment 如下
- diablo-frontent
  devops的前端组件，负责页面的渲染输出。
  启动时，会使用 diabloFrontend 容器作为 init container， 将前端的静态页面初始化到 alauda-console 容器中。 alauda-console 将会用来服务前端的静态页面。
  更多关于 alauda-console的信息 可参考 http://confluence.alaudatech.com/pages/viewpage.action?pageId=39336125
- diablo-backend
  devops的高级api组件， 前端的API请求， 会先经过diabloBackend 的一层处理， 然后将 请求转发给devops-apiserver(kubernetes apiserver)
- docs
  devops文档组件

## 其他chart
- jenkins chart
  负责 jenkins组件的按照部署升级，同时包含了 若干 构建使用的 slave。
- gitlab-ce chart
  负责 gitlab 社区版的部署升级
- harbor chart
  负责 harbor 的部署升级

设计与架构参考：http://confluence.alaudatech.com/pages/viewpage.action?pageId=39322566