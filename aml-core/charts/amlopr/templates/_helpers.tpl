{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}

{{- define "amlopr.image" -}}
{{- $repositoryName := .Values.global.images.amlopr.repository -}}
{{- $tag := .Values.global.images.amlopr.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "amlfilebrowser.image" -}}
{{- $repositoryName := .Values.global.images.amlfilebrowser.repository -}}
{{- $tag := .Values.global.images.amlfilebrowser.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "modelswaggerui.image" -}}
{{- $repositoryName := .Values.global.images.modelswaggerui.repository -}}
{{- $tag := .Values.global.images.modelswaggerui.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "tensorboardui.image" -}}
{{- $repositoryName := .Values.global.images.tensorboard.repository -}}
{{- $tag := .Values.global.images.tensorboard.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "jenkinstool.image" -}}
{{- $repositoryName := .Values.global.images.jenkinsTool.repository -}}
{{- $tag := .Values.global.images.jenkinsTool.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "jnlp.image" -}}
{{- $repositoryName := .Values.global.images.jnlp.repository -}}
{{- $tag := .Values.global.images.jnlp.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}