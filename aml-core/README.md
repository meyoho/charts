# Heml Chart for AML business cluster

## 介绍

aml-core用于aml的业务集群部署，包含了aml业务集群controller以及kubeflow的一系列controller

## 环境要求

- k8s >= 1.13
- k8s Ingress Controller

## 安装charts

```
$ helm install . --name ${name} --namespace ${namespace} \
--set global.ingress.host=${ingresshost} \
--set global.registry.address=${regAddress} 
```


## 卸载charts

注意: 删除crd会导致对应资源的删除

```
$ helm delete ${name}  --purge & \
kubectl delete crd tfjobs.kubeflow.org pytorchjobs.kubeflow.org &\
kubectl delete clusterrolebindings.rbac.authorization.k8s.io "aml-bus:${namespace}:cluster-admin"
```

## 批调度配置

*需要先安装kube-batch调度器，使用volcano chart进行安装,安装参考volcano中的说明*

安装
```
$ helm install . --name ${name} --namespace ${namespace}  --set tf-job.operator.enableGangScheduling=true \
--set global.ingress.host=${ingresshost} \
--set global.registry.address=${regAddress} 
```

已安装非批调度，升级
```
$ helm upgrade . ${name}  --set tf-job.operator.enableGangScheduling=true
```

## 备注
1. ingress查询
```
$ cat /etc/kubernetes/kubelet.conf | grep server: | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
```
