{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "amlcore.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{/* Helm required labels */}}
{{- define "amlcore.labels" -}}
heritage: {{ .Release.Service }}
release: {{ .Release.Name }}
chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
app: "{{ template "amlcore.name" . }}"
mode: business
{{- end -}}

{{/* matchLabels */}}
{{- define "amlcore.matchLabels" -}}
release: {{ .Release.Name }}
app: "{{ template "amlcore.name" . }}"
mode: business
{{- end -}}

