# Welcome to [apprelease](https://bitbucket.org/mathildetech/sentry) 👋

sentry使用文档
http://confluence.alauda.cn/pages/viewpage.action?pageId=56996488


例子
```yaml
apiVersion: operator.alauda.io/v1alpha1
kind: AppRelease
metadata:
  annotations:
  name: cpaas-appreleases
  namespace: alauda-system
spec:
  version: v0.0.8
  chart:
    repository: https://onechao.github.io/appreleases
    name: cpaas-appreleases
    version: v2.5-b.2
  values:
    global:
      host: k8s.alauda.io
      scheme: https
      namespace: alauda-system
      secret:
        tlsCrt: Cg==
        tlsKey: Cg==
    sentry:
      namespace: alauda-system
      version: v0.0.8



```