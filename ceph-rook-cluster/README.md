# ceph cluster deployment charts using rook operator

## installation

请参考http://confluence.alaudatech.com/pages/viewpage.action?pageId=23369723

helm repo add rook-beta https://charts.rook.io/beta

helm install --namespace rook-ceph-system --name rook-ceph rook-beta/rook-ceph --set rbacEnable=true

修改operator角色权限

重启operator

安装ceph-rook-cluster
自定义以下选项

```yaml
# Default values for ceph-rook-cluster.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

#use all device in cluster
useAllDevices: true
#specify device list in cluster when useAllDevices is enabled
devicefilter : vd.

# specify device list when useAllDevices is false
node:
  nodes:
    - devices:  
      - name: vdd
      name: 192.168.0.12
    - devices:
      - name: vdc
      name: 192.168.0.17
#install crd when prometheus operator is installed.
prometheusCRDEnable: false
#enable rados rest gateway(default:true)
radgwEnable: true
#enable cephfs (default:true)
cephfsEnable: true
#enable rbd (default:true)
rbdEnable: true
#install toolbox
toolboxEnable: true
```