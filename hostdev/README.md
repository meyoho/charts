# Heml Chart for HostDev

## 介绍

[hostdev plugin](https://github.com/honkiko/k8s-hostdev-plugin) is a device plugin for Kubernetes to configure devices under host /dev into PODs through device cgroup. Now is used in timatrix

## 安装charts

```
$ helm install . --name ${name} --set global.namespace=${namespace} 
```


## 卸载charts

```
$ helm delete ${name}  --purge 
```
