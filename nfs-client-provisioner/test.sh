#!/bin/bash

helm install release/nfs-client-provisioner \
        --version v2.9.13 \
        --name  nfs-client-provisioner \
        --set global.namespaces=cpaas-system \
        --namespace cpaas-system \
        --set defaultRootName=alauda \
        --set global.registry.address=index.alauda.cn \
        --set global.images.nfsclient.repository=claas/nfs-client-provisioner \
        --set global.images.nfsclient.tag=v3.1.0-k8s1.11 \
        --set storageClass.create=false \
        --set serviceAccount.create=false \
        --set nfs.server=192.168.16.171 \
        --set nfs.path=/kubernetes \
        --dry-run \
        --debug > 1.out