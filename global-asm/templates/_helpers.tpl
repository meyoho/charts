{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}

{{- define "ingressHost" -}}
{{- $scheme :=  .Values.global.scheme -}}
{{- printf "%s://%s" $scheme .Values.global.host | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Helm required labels */}}
{{- define "asm.labels" -}}
heritage: {{ .Release.Service }}
release: {{ .Release.Name }}
chart: {{.Chart.Name}}
{{- end -}}
 