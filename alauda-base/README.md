global:
  registry:
    address: index.alauda.cn  镜像仓库
  etcdSecret:
    ca:
      name: null              当 etcd 是 https的时候， 设置 该字段为 当前namespace下保存的 etcd ca的 secret 名称
    peer:
      name: null              当 etcd 是 https的时候， 设置 该字段为 当前namespace下保存的 etcd peer的 secret 名称
  namespace: alauda-system    部署的目标 namespace
  labelBaseDomain: alauda.io  label的域名后缀
  auth:
    default_admin: admin@alauda.io                                  默认管理原邮箱
    default_admin_md5: 3608afda9b9767bbe9c309dd1dc23001             默认管理员邮箱做MD5加密, md5({default_admin})
    default_userbinding_name: 2036591bdd92f723b426a064c92a01a8      默认userbinding cr name，name为MD5字符串. 字符串拼接规则: {default_admin}-acp-platform-admin-platform。例如 md5(admin@alauda.io-acp-platform-admin-platform)
registrySecrets: []           用来拉取镜像的 secret 名称， 需要指定为 registrySecrets.name
devopsApiServer:
  etcdServer: http://etcd-service:2379       当前集群etcd的地址
  livenessPort: 8080
  readinessPort: 8080
  apiQPS: 100                                访问k8s api 时的限流设置
  apiBurst: 125                              访问k8s api 时的限流峰值，超过该值，则会延迟处理。
  multiClusterHost: https://erebus:443       erebus 地址
furion:
  chartmuseumUrl:  http://chartmuseum:chartmuseum@10.0.128.95:8088   chartmuseum 地址，需要带用户名密码，http
  kaldrUrl: http://10.0.128.95:7000                                  kaldr 地址，提供 yum apt zypper 源，需要带 http
  downloadServerUrl:  http://10.0.128.95:8000                        download server地址，提供 ake 和 setup 脚本下载
  registryUrl: 10.0.128.95:60080                                     镜像仓库地址，不带http
  apiGateway: https://10.0.128.95                                    apigateway 地址
  globalResourceNamespace: alauda-system                             资源所在ns。默认为alauda-system
  globalResourcePrefix: cluster-                                     资源前缀。比如cluster对应的secret。 为了防止重名，会增加一个前缀。
alaudaConsole:
  replicas: 1                                                        副本数
  labelBaseDomain: alauda.io                                         label的默认domain
  apiAddress: https://129.28.182.197                                 apigateway的地址
  oidcIssuerUrl: https://129.28.182.197/dex                          dex地址
  oidcRedirectUrl: https://129.28.182.197                            登陆跳转地址,不要带最后的/
  oidcClientId: alauda-auth                                          oidc client id
  oidcClientSecret: ZXhhbXBsZS1hcHAtc2VjcmV0                         oidc client secret
nortrom:
  esHost: http://alaudaes:es_password_1qaz2wsx@alauda-elasticsearch:9200 es集群地址
  kafkaHost: 10.0.129.98:9092,10.0.128.19:9092,10.0.128.138:9092         kafka brokers
