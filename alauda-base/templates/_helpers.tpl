{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "base.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "base.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* Helm required labels */}}
{{- define "base.annotations" -}}
alauda.io/product: Alauda Base
{{- end -}}

{{/* Helm required labels */}}
{{- define "base.labels" -}}
heritage: {{ .Release.Service }}
release: {{ .Release.Name }}
chart: {{.Chart.Name}}-{{.Chart.Version}}-{{.Chart.AppVersion}}
{{- end -}}

{{- define "base.devopsApiServer.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.devopsApiServer.repository -}}
{{- $tag := .Values.global.images.devopsApiServer.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}

{{- define "base.devopsPipelineTemplates.image" -}}
{{- $registryAddress :=  .Values.global.registry.address -}}
{{- $repositoryName := .Values.global.images.devopsPipelineTemplates.repository -}}
{{- $tag := .Values.global.images.devopsPipelineTemplates.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}