#!/usr/bin/env bash

echo "running kustomize" 1>&2

kustomize build . > templates/cluster/cluster-registry.yaml

echo "running helm lint" 1>&2

helm lint .

echo "running sed to replace replicas"
sed -i "" "s/'{{\.Values\.global\.replicas}}'/{{.Values.global.replicas}}/g" templates/cluster/cluster-registry.yaml