auth:
  default_admin: admin@alauda.io                平台默认管理员账号
  is_install_auth: true                         是否安装auth crd及组件, 默认true。 在global集群安装cluster-base charts时，避免重复安装auth
  相关crd及deployment时冲突需手动指定为false
