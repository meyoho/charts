nevermore:
  apiGatewayHost: https://10.0.128.76     日志的推送地址
  apiGatewayUrl: /v3/logs/callback        日志的推送地址的url, 除非ingress规则有改动，否则就默认这个。
  token:  <token>                         用于日志推送的token
  region: global                          用于设置当前daemonset 的所在部署集群名称，必须唯一
command: 
  需要根据实际部署去指定
  helm install --name alauda-nevermore --set nevermore.region=<region> --set nevermore.apiGatewayHost=<apigateway> --set token=<token> .

## 支持OCP

与部署标准K8S的区别在于需要设置两个值:

- `global.isOCP=true`, 用于解决nevermore中写position文件权限等问题
- `nevermore.containerEngine=crio`，对于ocp3.x版本不需要设置，只有当ocp4开始才需要设置这个值

完整的安装命令

```bash
region_name=<openshift4.2>
apigateway=<1.1.1.1>
namespace=<cpass-system>
helm install --name alauda-log-agent \
    --namespace $namespace \
    --set neevermore.region=$region_name \
    --set nevermore.apiGatewayHost=https://$apigateway \
    --set global.namespace=$namespace \
    --set global.isOCP=true \
    --set nevermore.containerEngine=crio
```
