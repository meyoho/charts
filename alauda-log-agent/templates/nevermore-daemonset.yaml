apiVersion: apps/v1
kind: DaemonSet
metadata:
  labels:
    chart: "{{.Chart.Name}}-{{.Chart.Version}}-{{.Chart.AppVersion}}"
    service_name: nevermore
  name: nevermore
  namespace: {{ .Values.global.namespace }}
spec:
  updateStrategy:
    type: RollingUpdate
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      name: nevermore
  template:
    metadata:
      labels:
        name: nevermore
        project.{{ .Values.global.labelBaseDomain }}/name: nevermore
        service_name: nevermore
        {{ .Values.global.labelBaseDomain }}/product: "Platform-Center"
    spec:
      containers:
      - env:
        - name: APIGATEWAY_ENDPOINT
          value: {{ .Values.nevermore.apiGatewayHost }}
        - name: AUTHENTICATION
          value: bearer
        {{ if eq .Values.nevermore.token "token" }}
        - name: AUTH_TOKEN
          valueFrom:
            secretKeyRef:
              name: tke-cluster-global
              key: token
        {{ else }}
        - name: AUTH_TOKEN
          value: {{ .Values.nevermore.token }}
        {{ end }}
        - name: RELOAD_FLUENTD_MODE
          value: restart
        - name: APIGATEWAY_URL
          value: {{ .Values.nevermore.apiGatewayUrlPrefix }}/logs
        - name: APIGATEWAY_AUDIT_URL
          value: {{ .Values.nevermore.apiGatewayUrlPrefix }}/audits
        - name: APIGATEWAY_EVENT_URL
          value: {{ .Values.nevermore.apiGatewayUrlPrefix }}/events
        - name: RELOAD_INTERVAL
          value: "600"
        - name: ROTATE_FILE_SIZE
          value: 512M
        - name: DOCKER_FILE_PATH
          value: /alauda/log
        - name: FLUSH_INTERVAL
          value: 3s
        - name: RETRY_LIMIT
          value: "5"
        - name: RETRY_WAIT
          value: 4s
        - name: HTTP_RETRY_STATUSES
          value: 500,403,503,504
        - name: HTTP_READ_TIMEOUT
          value: "60"
        - name: HTTP_OPEN_TIMEOUT
          value: "30"
        - name: BUFFER_QUEUE_LIMIT
          value: "128"
        - name: BUFFER_CHUNK_LIMIT
          value: 64m
        - name: REGION_NAME
          value: {{ .Values.nevermore.region }}
        - name: NAMESPACE
          value: "alauda"
        - name: PROVIDER_NAMESPACES
          value: {{ .Values.global.namespace }}
        - name: LABEL_BASEDOMAIN
          value: {{ .Values.global.labelBaseDomain }}
        - name: __ALAUDA_REGION_ID__
          value: "donotcare"
        - name: __ALAUDA_REGION_NAME__
          value: {{ .Values.nevermore.region }}
        - name: CLEAN_INTERVAL
          value: "0.1"
        - name: PROJECT_LABEL
          value: project.{{ .Values.global.labelBaseDomain }}/name
        - name: NS_PROJECT_LABEL
          value: {{ .Values.global.labelBaseDomain }}/project
        - name: APP_LABEL
          value: app.{{ .Values.global.labelBaseDomain }}/name
        - name: SERVICE_LABEL
          value: service_name
        - name: PRODUCT_LABEL
          value: {{ .Values.global.labelBaseDomain }}/product
        - name: __ALAUDA_SERVICE_ID__
          value: nevermore.daemonsets
        - name: __ALAUDA_SERVICE_NAME__
          value: nevermore.daemonsets.default
        - name: __ALAUDA_APP_NAME__
          value: official-log
        - name: CONTAINER_ENGINE
          value: {{ .Values.nevermore.containerEngine }}
        {{ if .Values.global.isOCP }}
        securityContext:
          privileged: true
          runAsUser: 0
        {{ end }}
        image: {{ .Values.global.registry.address }}/{{ .Values.global.images.nevermore.repository }}:{{ .Values.global.images.nevermore.tag }}
        imagePullPolicy: Always
        name: nevermore
        {{ if .Values.resources }}
        resources:
{{ toYaml .Values.resources | indent 10 }}
        {{ else }}
        resources:
          limits:
            cpu: "2"
            memory: 4Gi
          requests:
            cpu: 100m
            memory: 128Mi
        {{ end }}
        volumeMounts:
        - mountPath: /scan_dir/
          name: hostrootpath
        - mountPath: /var/log
          name: varlog
      dnsPolicy: ClusterFirst
      hostNetwork: true
      tolerations:
      - effect: NoSchedule
        operator: Exists
      volumes:
      - hostPath:
          path: /
          type: ""
        name: hostrootpath
      - hostPath:
          path: /var/log
          type: ""
        name: varlog
