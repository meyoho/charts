# Heml Chart for scheduler batch

## 介绍

volcano用于安装kube-batch批调度器，目前被aml业务集群所使用

## 环境要求

- k8s >= 1.13

## 安装charts

```
$ helm install . --name ${name} --namespace ${namespace}
```


## 卸载charts

注意: 删除crd会导致对应资源的删除

```
$ helm delete ${name}  --purge &\
kubectl delete crd commands.bus.volcano.sh  jobs.batch.volcano.sh queues.scheduling.incubator.k8s.io  podgroups.scheduling.incubator.k8s.io  &&\
kubectl delete mutatingwebhookconfigurations.admissionregistration.k8s.io ${name}-mutate-job &&\
kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io ${name}-validate-job 
```

