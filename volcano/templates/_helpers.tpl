{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}

{{- define "controller.image" -}}
{{- $repositoryName := .Values.global.images.controller_image.repository -}}
{{- $tag := .Values.global.images.controller_image.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "scheduler.image" -}}
{{- $repositoryName := .Values.global.images.scheduler_image.repository -}}
{{- $tag := .Values.global.images.scheduler_image.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}

{{- define "admission.image" -}}
{{- $repositoryName := .Values.global.images.admission_image.repository -}}
{{- $tag := .Values.global.images.admission_image.tag -}}
{{- printf "%s/%s:%s" .Values.global.registry.address $repositoryName $tag -}}
{{- end -}}