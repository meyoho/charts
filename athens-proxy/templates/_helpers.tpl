{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}
{{- define "readinessPath" -}}
{{- if contains "v0.2.0" .Values.global.images.athens.tag -}}/{{- else -}}/readyz{{- end -}}
{{- end -}}

{{- define "athens.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.athens.repository .Values.global.images.athens.tag -}}
{{- end -}}

{{- define "alpine.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.alpine.wget.repository .Values.global.images.alpine.tag -}}
{{- end -}}

{{- define "jaeger.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.jaeger.repository .Values.global.images.jaeger.tag -}}
{{- end -}}
