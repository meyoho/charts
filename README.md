# [Moved to Gitlab](https://gitlab-ce.alauda.cn/alauda/charts)

# Charts repository

This repository stores all the charts for Alauda Kubernetes related products

The process of releasing a new chart happens automatically using the Jenkins pipeline. In order to do that the following steps should be taken:

 - The pipeline should be able to update one chart at a time, even one component at a time
 - Increase the chart version automatically
 - Tar the chart and update the official chart repository
 - In order to update the charts a very simple interface should be used to read and update the Chart.yaml and values.yaml and commit the changes

# Chart versioning

为了保证charts的流水线可以正常执行持续更新chart的版本需要关注chart版本号的格式：各个chart的 `Charts.yaml` 文件中的 `version` 字段
流水线应该会自动更新版本，所以如果格式正确的话，后面其它的更新由流水线来执行，不用人为干涉。

## Master分支
第一次构建时需要提供一个基础版本号。格式为 semver `v2.1.0` 或 build 格式 `v2.1-b.1`

## release-x.x分支
第一次构建时需要提供一个基础版本号。格式为 semver `v2.1.0` 或 rc 格式 `v2.1-rc.1`

# Adding new charts

## Create the chart

When creating the chart please follow the conventions in this repo. The most important factor is the `values.yaml` file should contain the docker images used following this specific format:

```
# ... all the other values
global:
  registry:
    address: index.alauda.cn
  images:
    <component name>:
      # repository path
      repository: alaudak8s/devops-apiserver
      # tag used for image
      tag: v1.4.62
```

## Change the chartsupdate.yaml

In order to automatically update a chart's and it's components version update `chartsupdate.yaml` adding your chart

```
charts:
    <your chart name>:
        groups:
        # group of product that the chart belongs to
        # currently we have acp2.0, ace3.0 and internal
        # if blank will be ignored in the regular process
        # unless a "all-in" approach is used (check triggering charts pipeline docs)
        - acp2.0
        - ace3.0
        chartVersion:
        - <filepath to Chart.yaml>
        <component>:
            # type can be same or split
            # if same is used the same tag will be given to all files under files
            # if split than image tag's value should be a list of tags separated by comma i.e v1.0,v2.0 and it will 
            # attribute and update accordingly (first to the first, second to the second, etc)
            type: same 
            files:
            - file: <path to values file>
              key: <yaml key to image value>
            labels:
              # the value of smoking: you should commutate with QA to decide what value you should use. eg. none is skip all cases, BAT indicates all cases.
              smoking: <test case label that you want to run>
            
```

### 业务集群中的chart

为了满足通过流水线部署业务集群的chart需要在 `chartsupdate.yaml` 添加如下环境信息 （environments字段）：

注意：如果添加了environments中的环境字段与cluster信息（例如 int）会自动使用定义的环境来部署，并不会在global部署。如果需要同时在多个集群部署，包括global，需要先把global接入并加多个cluster信息。

```
charts:
    <your chart name>:
        chartVersion:
        - <filepath to Chart.yaml>
        environments:
          int: ## 环境名字，目前只支持 int 和 staging
          - name: high ## 集群名称（clusters.clusterregistry.k8s.io 资源)
            namespace: alauda-system ## 资源实例的namespace
          staging:
          - name: high ## 集群名称（clusters.clusterregistry.k8s.io 资源)
            namespace: alauda-system ## 资源实例的namespace
```
- 使用captain 部署业务集群时的重名问题

在某些情况下， 我们可能会将单个chart 同时部署到同一个环境的多个集群下。 在使用helm的时候，release 的资源都是在各个集群上维护的；在使用captain的时候， helmrequest的资源，都在 global 上保存。
当我们使用chart的名字作为 release 的名称时， 如果是 helm 部署则没有问题， 如果是captain 部署，则会导致 helmrequest重名的问题。
为了解决上述问题， 可以在某个集群下，指定目标helmrequest的名字，如果指定了， 则使用 该名称来更新。
```
charts:
    <your chart name>:
        chartVersion:
        - <filepath to Chart.yaml>
        environments:
          int: ## 环境名字，目前只支持 int 和 staging
          - name: high ## 集群名称（clusters.clusterregistry.k8s.io 资源)
            namespace: alauda-system ## 资源实例的namespace
            deploy:
              hr: <your target helmrelease name>
```
这里的 目标环境的 helmrelease 名称 符合以下规则：
1. 如果指定了 `envirionments.xx[x].deploy.hr`, 则优先使用该名称
2. 如果没有指定 `envirionments.xx[x].deploy.hr`， 但需要部署到目标环境的业务集群，则使用规则 <环境名称>-<chart 名称>
3. 如果不需要部署到目标环境的业务集群，也就是没有指定envrionments配置， 则使用 chart名称作为 helmrequest 名称。



## Change the chartcode.yaml

In this file it is necessary to add each component's source code's git address

```
charts:
    <your chart name>:
        <component>: <git address>
```

The component name should be the same used in the `values.yaml` for the image tag


## Testing your chart

### Testing chart using helm lint
Testing the chart should be done manually using helm. Please check helm's documentation for more information

### Testing chart using conftest and policy
helm lint 只能对 chart 进行格式校验，[conftest](https://github.com/instrumenta/conftest) 可以基于策略规则对 chart 进行逻辑层面的校验。    

* 根据[安装文档](https://github.com/instrumenta/conftest#installation)安装 conftest
* 安装 conftest 针对 helm 的插件：`helm plugin install https://github.com/instrumenta/helm-conftest`
* 使用如下命令对 chart 进行规则校验：
    * `make check`: 基于 `policy` 目录下的策略对所有 charts 进行校验
    * `make edge-check`：基于 `policy_edge` 目录下的策略对所有 charts 进行校验
    * `make check-<chart-name>`: 基于 `policy` 目录下的策略对指定的 chart 进行校验
    * `make edge-check-<chart-name>`: 基于 `policy_edge` 目录下的策略对指定的 chart 进行校验

更多相关资料：

* [Using Conftest and Kubeval with Helm](https://garethr.dev/2019/08/using-conftest-and-kubeval-with-helm/)  
* [rego language](https://www.openpolicyagent.org/docs/latest/policy-language)

## Notice

- DO NOT add dynamic variables in mathLabels, It will cause error when execute `helm upgrade`
- DO NOT create resource directly on int
- Add `helm.sh/hook: crd-install  helm.sh/hook-delete-policy: before-hook-creation` when you add crd

