{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "postgresql.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "postgresql.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Return the appropriate apiVersion for networkpolicy.
*/}}
{{- define "postgresql.networkPolicy.apiVersion" -}}
{{- if and (ge .Capabilities.KubeVersion.Minor "4") (le .Capabilities.KubeVersion.Minor "6") -}}
"extensions/v1beta1"
{{- else if ge .Capabilities.KubeVersion.Minor "7" -}}
"networking.k8s.io/v1"
{{- end -}}
{{- end -}}


{{- define "postgresql.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.postgresql.repository .Values.global.images.postgresql.tag -}}
{{- end -}}

{{- define "exporter.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.exporter.repository .Values.global.images.exporter.tag -}}
{{- end -}}

{{- define "postgresql.nodeSelector" -}}
{{- if .Values.nodeSelector }}
nodeSelector:
{{ toYaml .Values.nodeSelector | indent 2 }}
{{- else }}
{{- if .Values.database.persistence.host.nodeName }}
nodeSelector:
  kubernetes.io/hostname: {{ .Values.database.persistence.host.nodeName }}
{{- end }}
{{- end }}
{{- end }}

{{- define "postgresql.volume" -}}
{{- if .Values.database.persistence.enabled }}
persistentVolumeClaim:
  claimName: {{ .Values.database.persistence.existingClaim | default (printf "%s" (include "postgresql.fullname" .)) }}  
{{- else }}
{{- if and (.Values.database.persistence.host.nodeName) (.Values.database.persistence.host.path) }}
hostPath:
  path: {{ .Values.database.persistence.host.path }}
  type: DirectoryOrCreate
{{- else }}
emptyDir: {}
{{- end }}
{{- end }}
{{- end }}