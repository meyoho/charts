# SonarQube

> **charts目录下的tgz包是从本仓库对应的目录打包出来的，并非官方包**

[SonarQube](https://www.sonarqube.org/) is an open sourced code quality scanning tool.

> sonarqube本身的持久化存储不建议使用，会出现权限不足的问题。（用官方的pvc做法也一样）

> 暂不支持Mysql 所以数据库类型不要填写mysql

## Introduction

This chart bootstraps a SonarQube instance with a PostgreSQL database.

## Prerequisites

- Kubernetes 1.6+

## Installing the chart

To install the chart:

```bash
$ helm install stable/sonarqube
```

The above command deploys Sonarqube on the Kubernetes cluster in the default configuration. The [configuration](#configuration) section lists the parameters that can be configured during installation.

The default login is admin/admin.

## Uninstalling the chart

To uninstall/delete the deployment:

```bash
$ helm list
NAME       	REVISION	UPDATED                 	STATUS  	CHART          	NAMESPACE
kindly-newt	1       	Mon Oct  2 15:05:44 2017	DEPLOYED	sonarqube-0.1.0	default
$ helm delete kindly-newt
```

## Create the chart

When creating the chart please follow the conventions in this repo. The most important factor is the `values.yaml` file should contain the docker images used following this specific format:

```
# ... all the other values
global:
  registry:
    address: harbor-b.alauda.cn
  images:
    <component name>:
      # repository path
      repository: devops/devops-apiserver
      # tag used for image
      tag: v1.4.62
```

## Configuration

The following table lists the configurable parameters of the Sonarqube chart and their default values.

| Parameter                                   | Description                               | Default                                    |
| ------------------------------------------  | ----------------------------------------  | -------------------------------------------|
| `image.pullPolicy`                          | Image pull policy                         | `IfNotPresent`                             |
| `image.pullSecret`                          | imagePullSecret to use for private repository      |                                   |
| `command`                                   | command to run in the container           | `nil` (need to be set prior to 6.7.6, and 7.4)      |
| `securityContext.fsGroup`                   | Group applied to mounted directories/files|  `999`                                     |
| `ingress.enabled`                           | Flag for enabling ingress                 | false                                      |
| `ingress.labels`                            | Ingress additional labels                 | `{}`                                       |
| `ingress.hosts[0].name`                     | Hostname to your SonarQube installation   | `sonar.organization.com`                   |
| `ingress.hosts[0].path`                     | Path within the URL structure             | /                                          |
| `ingress.tls`                               | Ingress secrets for TLS certificates      | `[]`                                       |
| `livenessProbe.sonarWebContext`             | SonarQube web context for livenessProbe   | /                                          |
| `readinessProbe.sonarWebContext`            | SonarQube web context for readinessProbe  | /                                          |
| `service.type`                              | Kubernetes service type                   | `LoadBalancer`                             |
| `service.labels`                            | Kubernetes service labels                 | None                                       |
| `service.annotations`                       | Kubernetes service annotations            | None                                       |
| `service.loadBalancerSourceRanges`          | Kubernetes service LB Allowed inbound IP addresses | 0.0.0.0/0                            |
| `service.loadBalancerIP`                    | Kubernetes service LB Optional fixed external IP   | None                                       |
| `persistence.enabled`                       | Flag for enabling persistent storage      | false                                      |
| `persistence.existingClaim`                 | Do not create a new PVC but use this one  | None                                       |
| `persistence.storageClass`                  | Storage class to be used                  | "-"                                        |
| `persistence.accessMode`                    | Volumes access mode to be set             | `ReadWriteOnce`                            |
| `persistence.size`                          | Size of the volume                        | None                                     |
| `sonarProperties`                           | Custom `sonar.properties` file            | None                                       |
| `customCerts.enabled`                       | Use `customCerts.secretName`              | false                                      |
| `customCerts.secretName`                    | Name of the secret which conatins your `cacerts` | false                                      |
| `sonarSecretKey`                            | Name of existing secret used for settings encryption | None                            |
| `sonarProperties`                           | Custom `sonar.properties` file            | `{}`                                       |
| `database.type`                             | Set to "mysql" to use mysql database       | `postgresql`|
| `postgresql.enabled`                        | Set to `false` to use external server / mysql database     | `true`                                     |
| `postgresql.postgresServer`                 | Hostname of the external Postgresql server| `null`                                     |
| `postgresql.postgresPasswordSecret`         | Secret containing the password of the external Postgresql server | `null`              |
| `postgresql.postgresUser`                   | Postgresql database user                  | `sonarUser`                                |
| `postgresql.postgresPassword`               | Postgresql database password              | `sonarPass`                                |
| `postgresql.postgresDatabase`               | Postgresql database name                  | `sonarDB`                                  |
| `postgresql.service.port`                   | Postgresql port                           | `5432`                                     |
| `mysql.enabled`                             | Set to `false` to use external server / postgresql database        | `false`                                     |
| `mysql.mysqlServer`                         | Hostname of the external Mysql server     | `null`                                     |
| `mysql.mysqlPasswordSecret`                 | Secret containing the password of the external Mysql server | `null`                   |
| `mysql.mysqlUser`                           | Mysql database user                       | `sonarUser`                                |
| `mysql.mysqlPassword`                       | Mysql database password                   | `sonarPass`                                |
| `mysql.mysqlDatabase`                       | Mysql database name                       | `sonarDB`                                  |
| `mysql.mysqlParams`                         | Mysql parameters for JDBC connection string     | `{}`                                 |
| `mysql.service.port`                        | Mysql port                                | `3306`                                     |
| `resources`                                 | Sonarqube Pod resource requests & limits  | `{}`                                       |
| `affinity`                                  | Node / Pod affinities                     | `{}`                                       |
| `nodeSelector`                              | Node labels for pod assignment            | `{}`                                       |
| `hostAliases`                               | Aliases for IPs in /etc/hosts             | `[]`                                       |
| `tolerations`                               | List of node taints to tolerate           | `[]`                                       |
| `plugins.install`                           | List of plugins to install                | `[]`                                       |
| `plugins.resources`                         | Plugin Pod resource requests & limits     | `{}`                                       |
| `plugins.initContainerImage`                | Change init container image               | `[]`                                       |
| `plugins.deleteDefaultPlugins`              | Remove default plugins and use plugins.install list | `[]`                             |
| `podLabels`                                 | Map of labels to add to the pods          | `{}`                                       |

You can also configure values for the PostgreSQL / MySQL database via the Postgresql [README.md](https://github.com/kubernetes/charts/blob/master/stable/postgresql/README.md) / MySQL [README.md](https://github.com/kubernetes/charts/blob/master/stable/mysql/README.md)

For overriding variables see: [Customizing the chart](https://docs.helm.sh/using_helm/#customizing-the-chart-before-installing)

### Use custom `cacerts`

In environments with air-gapped setup, especially with internal tooling (repos) and self-signed certificates it is required to provide an adequate `cacerts` which overrides the default one:

1. Create a yaml file `cacerts.yaml` with a secret that contanins the `cacerts`

   ```yaml
   apiVersion: v1
   kind: Secret
   metadata:
     name: my-cacerts
   data:
     cacerts: |
       xxxxxxxxxxxxxxxxxxxxxxx
   ```

2. Upload your `cacerts.yaml` to a secret with the key you specify in `secretName` in the cluster you are installing Sonarqube to.

   ```shell
   $ kubectl apply -f cacerts.yaml
   ```

3. Set the following values of the chart:

   ```yaml
   customCerts:
      ## Enable to override the default cacerts with your own one
      enabled: false
      secretName: my-cacerts
   ```

# alauda 自定义

  添加自定义插件
  `.Values.plugins.useDefaultPluginsPackage`
  
  
## SSO 使用及配置
设置 SonarQube 实现 SSO 功能，需要增加以下配置


| 参数名称                  | 描述                        | 
| -----------------------    | ---------------------------------- | 
|`plugins.install`|需要安装的插件|
| `oidc.enable`       | 是否打开 oidc 功能 | 
| `oidc.clientID`  | OIDC 提供方 clientID | 
| `oidc.clientSecret` | OIDC 提供方 clientSccret | 
| `oidc.issuer` | OIDC 提供方地址 | 
| `oidc.scope`       | 授权访问用户的详细信息 | 
| `oidc.serverURL`       | SonarQube 地址 | 
|`odic.verifyCert`|禁用 TLS 验证|
|`oidc.sonarqube.tlsCrt`| TLS 公钥证书 pem 格式|
| `oidc.sonarqube.forceAuthentication`       |Forcing user authentication prevents anonymous users from accessing the SonarQube UI, or project data via the Web API. Some specific read-only Web APIs, including those required to prompt authentication, are still available anonymously.  可填写 false |
| `oidc.sonarqube.loginStrategy`       | When the login strategy is set to 'Unique', the user's login will be auto-generated the first time so that it is unique. When the login strategy is set to 'Same as OpenID Connect login', the user's login will be the OpenID Connect provider's internal user ID. When the login strategy is set to 'Email', the user's login will be the OpenID Connect provider's user email. When the login strategy is set to 'Preferred username', the user's login will be the OpenID Connect provider's user name.    可填写 Email | 
| `oidc.sonarqube.response_types_supported`       | 可填写 '["code"\,"id_token"\,"token"]' |
| `oidc.sonarqube.subject_types_supported`       | 可填写 '["public"]' | 
| `oidc.sonarqube.token_endpoint_auth_methods_supported`       | 可填写 '["client_secret_basic"]' |
| `oidc.sonarqube.id_token_signing_alg_values_supported`       | 可填写 '["RS256"]' | 
| `oidc.sonarqube.claims_supported`       | 可填写 '["aud"\,"email"\,"email_verified"\,"exp"\,"iat"\,"iss"\,"locale"\,"name"\,"sub"]' |

oidc.sonarqube中的 参数可以通过 https://oidcprovider/.well-known/openid-configuration来获得，参数说明详见 ![参数说明](https://openid.net/specs/openid-connect-discovery-1_0.html)

示例
```
  --set plugins.install={"https://github.com/vaulttec/sonar-auth-oidc/releases/download/v1.0.4/sonar-auth-oidc-plugin-1.0.4.jar"} \
  --set oidc.serverURL=http://xx.xx.xx.xx:31333 \
  --set oidc.enable=true \
  --set oidc.clientID=alauda-auth \
  --set oidc.issuer=https://192.xx.xx.xx/dex \
  --set oidc.clientSecret=XXXXXXXXXXXXXX \
  --set oidc.scope='["openid"\,"email"\,"groups"\,"profile"\,"offline_access"]'

  --set oidc.sonarqube.forceAuthentication=false \
  --set oidc.sonarqube.loginStrategy=Email \
  --set oidc.sonarqube.response_types_supported='["code"\,"id_token"\,"token"]'\
  --set oidc.sonarqube.subject_types_supported='["public"]' \
  --set oidc.sonarqube.token_endpoint_auth_methods_supported='["client_secret_basic"]' \
  --set oidc.sonarqube.id_token_signing_alg_values_supported='["RS256"]' \
  --set oidc.sonarqube.claims_supported='["aud"\,"email"\,"email_verified"\,"exp"\,"iat"\,"iss"\,"locale"\,"name"\,"sub"]' \
```

如果需要禁止 ssl 验证，需要将 https 公钥证书导入到 sonarqube 中，设置以下两个值即可
```
--set oidc.verifyCert=true
--set oidc.sonarqube.tlsCrt="$(cat /path/to/your/cert.pem | base64 -w 0)"
```

其中 cert.pem 即 oidc provider(一般就是指我们的 ACP 产品) 的公钥证书，如何导出可以参考 [https网站如何导出公钥](https://blog.csdn.net/lllkey/article/details/17526323)

将公钥文件保存到主机，通过 --set oidc.sonarqube.tlsCrt 设置文件路径，其中pem文件地址必须为绝对路径

### 判断是否成功
配置成功，在登录页面会提示 `Log in with OpenID...`，通过该选项跳转到 OIDC 提供商登录地址，登录可以返回sonar平台即可。

### 问题排查
* 如果发现问题，需要到sonarqube的容器中检查sonar.property的相关信息，检查是否是设置的值有问题
* 如果发现登录错误，可以查看sonarqube的pod日志，如果报错含有SSL问题，则需要增加上述提到的禁止ssl验证的操作

### 其他
* sonarqube 使用 adoptopenjdk:8u242-b08-jdk-hotspot-bionic 的 keytool 工具来做cacert工具的生成. 镜像同步通过构建环境 devops/javasdk-sonar 流水线完成，触发流水线时，触发 tag 使用 8u242-b08-jdk-hotspot-bionic 即可

