{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "sonarqube.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "sonarqube.fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
  Create a default fully qualified mysql/postgresql name.
  We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "postgresql.fullname" -}}
{{- printf "%s-%s" .Release.Name "postgresql" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- define "mysql.fullname" -}}
{{- printf "%s-%s" .Release.Name "mysql" | trunc 63 | trimSuffix "-" -}}
{{- end}}

{{/*
  Determine the hostname to use for PostgreSQL/mySQL.
*/}}
{{- define "postgresql.hostname" -}}
{{- if eq .Values.database.type "postgresql" -}}
{{- if .Values.postgresql.enabled -}}
{{- printf "%s-%s" .Release.Name "postgresql" | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s" .Values.postgresql.postgresServer -}}
{{- end -}}
{{- end -}}
{{- end -}}
{{- define "mysql.hostname" -}}
{{- if eq .Values.database.type "mysql" -}}
{{- if .Values.mysql.enabled -}}
{{- printf "%s-%s" .Release.Name "mysql" | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s" .Values.mysql.mysqlServer -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "sonarqube.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.sonarqube.repository .Values.global.images.sonarqube.tag -}}
{{- end -}}

{{- define "initSysctl.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.initSysctl.repository .Values.global.images.initSysctl.tag -}}
{{- end -}}

{{- define "plugins.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.pluginPackage.repository .Values.global.images.pluginPackage.tag -}}
{{- end -}}

{{- define "wget.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.wget.repository .Values.global.images.wget.tag -}}
{{- end -}}

{{- define "test.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.wget.repository .Values.global.images.wget.tag -}}
{{- end -}}

{{- define "openjdk.image" -}}
{{- printf "%s/%s:%s" .Values.global.registry.address .Values.global.images.openjdk.repository .Values.global.images.openjdk.tag -}}
{{- end -}}

{{- define "sonarqube.tls.secret" -}}
{{- printf "%s-tls" .Chart.Name -}}
{{- end -}}

{{- define "sonarqube.odic.providerConfiguration" -}}
{{- printf "{\"issuer\":\"%s\",\"authorization_endpoint\":\"%s/auth\",\"token_endpoint\":\"%s/token\",\"jwks_uri\":\"%s/keys\",\"response_types_supported\":%s,\"subject_types_supported\":%s,\"id_token_signing_alg_values_supported\":%s,\"scopes_supported\":%s,\"token_endpoint_auth_methods_supported\":%s,\"claims_supported\":%s}"  .Values.oidc.issuer  .Values.oidc.issuer  .Values.oidc.issuer .Values.oidc.issuer  .Values.oidc.sonarqube.response_types_supported  .Values.oidc.sonarqube.subject_types_supported .Values.oidc.sonarqube.id_token_signing_alg_values_supported .Values.oidc.scope .Values.oidc.sonarqube.token_endpoint_auth_methods_supported .Values.oidc.sonarqube.claims_supported -}}
{{- end -}}