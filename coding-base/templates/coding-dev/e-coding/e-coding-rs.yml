apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: {{ .Values.global.namespace }}
  name: e-coding
spec:
  selector:
    matchLabels:
      app: e-coding
  replicas: 1
  template:
    metadata:
      labels:
        app: e-coding
    spec:
      containers:
      - name: e-coding
        image: {{ .Values.global.registry.address }}/{{ .Values.global.images.e_coding.repository }}:{{ .Values.global.images.e_coding.tag }}
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8080
          name: http
        - containerPort: 20151
          name: grpc
        tty: true
        env:
        - name: "SSO_ALAUDA_ALAUDA_INFO_URL"
          value: {{ template "platform.ssoAddress" .}}
        - name: "LFS_STORAGE"
          value: "minio"
        - name: "LFS_ENDPOINT"
          valueFrom:
            configMapKeyRef:
              name: app-config
              key: minioPublicHost
        - name: "LFS_REGION"
          value: "us-east-1"
        - name: "LFS_BUCKET"
          valueFrom:
            configMapKeyRef:
              name: coding-minio-cm
              key: lfs_storage_name
        - name: "LFS_ACCESS_KEY"
          valueFrom:
            secretKeyRef:
              name: coding-minio-secret
              key: MINIO_ACCESS_KEY
        - name: "LFS_SECRET_ACCESS_KEY"
          valueFrom:
            secretKeyRef:
              name: coding-minio-secret
              key: MINIO_SECRET_KEY
        - name: "internal_interface_salt"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: CODING_INTERNAL_INTERFACE_SALT
        - name: "CODING_APP_CREDENTIAL_PUBLIC_KEY"
          valueFrom:
            secretKeyRef:
              name: app-keys
              key: CREDENTIAL_PUBLIC_KEY_STRING
        - name: "CODING_APP_CREDENTIAL_PRIVATE_KEY"
          valueFrom:
            secretKeyRef:
              name: app-keys
              key: CREDENTIAL_PRIVATE_KEY_STRING
        - name: "jdbc.slave.host"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: mysql
        - name: "cache.redis.host"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: redisHost
        - name: "qiniu_accessKey"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: QINIU_ACCESS_KEY
        - name: "qiniu_secretKey"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: QINIU_SECRET_KEY
        - name: "alipay_partner"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: ALIPAY_PARTNER
        - name: "alipay_key"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: ALIPAY_KEY
        - name: "weixin_appid"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: WEIXIN_APPID
        - name: "weixin_mch_id"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: WEIXIN_MCH_ID
        - name: "weixin_key"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: WEIXIN_KEY
        - name: "customer_service_password"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: CUSTOMER_SERVICE_PASSWORD
        # - name: "pages_domain"
        #   value: "static-pages.codingprod.net"
        # - name: "pages_manager"
        #   value: "http://host-6.codingprod.net:9290"
        - name: "ETCD_MACHINES"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: etcd
        - name: "oauth.gitlab.application_id"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: OAUTH_GITLAB_ID
        - name: "oauth.gitlab.secret"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: OAUTH_GITLAB_SECRET
        - name: "tencent.cos.secretKey"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: TENCENT_FILE_COS_SECRET_KEY
        - name: "oauth.wechat.apikey"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: OAUTH_WECHAT_APIKEY
        - name: "oauth.wechat.apisecret"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: OAUTH_WECHAT_APISECRET
        - name: "qcloud.console.token.secret-key"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: QCLOUD_CONSOLE_TOKEN_SECRET_KEY
        - name: "qcloud.console.cam.secret-id"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: QCLOUD_CONSOLE_CAM_SECRET_ID
        - name: "qcloud.console.cam.secret-key"
          valueFrom:
            secretKeyRef:
              name: app-secret
              key: QCLOUD_CONSOLE_CAM_SECRET_KEY
        - name: "minio.accessKey"
          valueFrom:
            secretKeyRef:
              name: coding-minio-secret
              key: MINIO_ACCESS_KEY
        - name: "minio.secretKey"
          valueFrom:
            secretKeyRef:
              name: coding-minio-secret
              key: MINIO_SECRET_KEY
        - name: "REDIS_HOST"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: operatorRedis
        - name: "REDISSON_HOST"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: operatorRedis
        - name: "CACHE_REDIS_HOST"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: operatorRedis
        - name: "pd.agileDevelopmentClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: agileClose
        - name: "pd.testManagementClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: testingClose
        - name: "pd.codeManagementClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: codeClose
        - name: "pd.continueIntegrationClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: ciClose
        - name: "pd.deploymentManagementClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: cdClose
        - name: "pd.artifactClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: artifactClose
        - name: "pd.wikiClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: wikiClose
        - name: "pd.statisticsClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: statisticsClose
        - name: "pd.codeAnalysisClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: codeAnalysisClose
        - name: "pd.apiDocsClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: apiDocClose
        - name: "pd.qtaClose"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: qtaClose
        - name: "FAKE_EMAIL_CLIENT"
          valueFrom:
            configMapKeyRef:
              name: e-coding-cm-1
              key: FAKE_EMAIL_CLIENT
        envFrom:
        - configMapRef:
            name: e-coding-cm2
        readinessProbe:
          tcpSocket:
            port: grpc
          initialDelaySeconds: 50
          # periodSeconds: 10
          timeoutSeconds: 3
        livenessProbe:
          httpGet:
            path: /metrics
            port: 8080
          initialDelaySeconds: 120
          periodSeconds: 10
          timeoutSeconds: 3
      nodeSelector:
        {{ .Values.global.nodeLabel }}: enabled
