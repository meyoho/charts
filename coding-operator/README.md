```yaml
global:
  labelBaseDomain: cpaas.io  label的域名后缀
  registry:
    address: index.alauda.cn 镜像仓库
  namespace: cpaas-system    operator 部署的命名空间
  replicas: 1                副本数
  images:
    coding-operator:
      repository: coding/coding-opeartor
      tag: v2.10-pr-2.11
  coding-operator:
    deploy_namespace: coding  coding 产品在业务集群部署的命名空间，如果命名空间不存在会自动创建
    chart_repo_endpoint: http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-chart-incubator-new.ace-default.cloud.myalauda.cn  coding 产品本身 chart 仓库地址
    sentry_timeout: "600"                  sentry 同步超时时间
    coding_version: v2.5                   coding 产品版本
    coding_node_num_limit: "3" coding      产品部署节点限制数
    coding_entry_point_prefix: codingcorp  coding 产品访问入口域名前缀
    coding_node_label_value: enabled       coding 产品部署节点选择标签的值
```
