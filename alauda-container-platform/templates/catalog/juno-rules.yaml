{{- if .Values.global.cluster.isGlobal }}
{{- $alert_base_domain := "" -}}
{{- $base_domain := "" -}}
{{- if .Values.global.labelBaseDomain -}}
{{- $alert_base_domain = printf ".%s" .Values.global.labelBaseDomain -}}
{{- $base_domain = printf "%s/" .Values.global.labelBaseDomain -}}
{{- end -}}
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  annotations:
    {{$base_domain}}description: Deployed by charts, it will be overwritten if charts updated,
      Please Do Not edit this resource.
    alert{{$alert_base_domain}}/notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
  labels:
    alert{{$alert_base_domain}}/cluster: '{{.Values.global.cluster.name}}'
    alert{{$alert_base_domain}}/kind: Deployment
    alert{{$alert_base_domain}}/name: juno
    alert{{$alert_base_domain}}/namespace: '{{.Values.global.namespace}}'
    alert{{$alert_base_domain}}/owner: System
    alert{{$alert_base_domain}}/project: '{{.Values.global.project}}'
    prometheus: '{{.Values.global.prometheusName}}'
  name: cpaas-juno-rules
  namespace: '{{.Values.global.namespace}}'
spec:
  groups:
  - name: general
    rules:
    - alert: pod.cpu.utilization-ns5en-c649889ed8206bef960823bf8b74ad38
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: sum by (pod_name) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",image!="",container_name!="POD"})
        / sum by (pod_name) (container_spec_cpu_quota{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",image!="",container_name!="POD"})
        * 100000 > {{.Values.alertRules.pod.cpu.utilization.threshold}}
      for: {{ .Values.alertRules.pod.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: pod.cpu.utilization
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: '>'
        alert_indicator_threshold: '{{.Values.alertRules.pod.cpu.utilization.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: pod.cpu.utilization-ns5en
        alert_project: '{{.Values.global.project}}'
        severity: Medium
    - alert: pod.memory.utilization-dmjem-9c58b5e25b75ba2cf2f5b7831dcfb921
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: ' sum by(pod_name) (container_memory_usage_bytes_without_cache{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}"})
        / sum by (pod_name) (container_spec_memory_limit_bytes{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",image!="",container_name!="POD"})
        >{{.Values.alertRules.pod.memory.utilization.threshold}}'
      for: {{ .Values.alertRules.pod.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: pod.memory.utilization
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: '>'
        alert_indicator_threshold: '{{.Values.alertRules.pod.memory.utilization.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: pod.memory.utilization-dmjem
        alert_project: '{{.Values.global.project}}'
        severity: Medium
    - alert: workload.cpu.utilization-0h9wv-61e17c4943f147295ff23ecddc4185c1
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: ' sum by (deployment_name) (container_cpu_usage_seconds_total_irate5m{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",image!="",container_name!="POD"})
        / sum by (deployment_name) (container_spec_cpu_quota{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",image!="",container_name!="POD"})
        * 100000 >{{.Values.alertRules.workload.cpu.utilization.threshold}}'
      for: {{ .Values.alertRules.workload.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: workload.cpu.utilization
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: '>'
        alert_indicator_threshold: '{{.Values.alertRules.workload.cpu.utilization.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: workload.cpu.utilization-0h9wv
        alert_project: '{{.Values.global.project}}'
        severity: Medium
    - alert: workload.memory.utilization-9gwaz-4ad4afeb033f9114f45d1a04ba154682
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: ' sum by (deployment_name) (container_memory_usage_bytes_without_cache{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}"})
        / sum by (deployment_name) (container_spec_memory_limit_bytes{namespace="{{.Values.global.namespace}}",pod_name=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",image!="",container_name!="POD"})
        >{{.Values.alertRules.workload.memory.utilization.threshold}}'
      for: {{ .Values.alertRules.workload.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: workload.memory.utilization
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: '>'
        alert_indicator_threshold: '{{.Values.alertRules.workload.memory.utilization.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: workload.memory.utilization-9gwaz
        alert_project: '{{.Values.global.project}}'
        severity: Medium
    - alert: workload.pod.restarted.count-wry0t-0e347a7e204edaa83060ed737357e725
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: sum (delta(kube_pod_container_status_restarts_total{namespace="{{.Values.global.namespace}}",pod=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}"}[5m]))>{{.Values.alertRules.workload.pod.restarted.count.threshold}}
      for: {{ .Values.alertRules.workload.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: workload.pod.restarted.count
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: '>'
        alert_indicator_threshold: '{{.Values.alertRules.workload.pod.restarted.count.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: workload.pod.restarted.count-wry0t
        alert_project: '{{.Values.global.project}}'
        severity: Medium
    - alert: workload.pod.status.phase.not.running-43b6y-9ad4be9ebd3313197d3d6be0a24ddaa9
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: sum(kube_pod_status_phase{namespace="{{.Values.global.namespace}}",pod=~"juno-[a-z0-9]{7,10}-[a-z0-9]{5}",phase!="Running"})>{{.Values.alertRules.workload.pod.status.phase.not.running.threshold}}
      for: {{ .Values.alertRules.workload.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: workload.pod.status.phase.not.running
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: '>'
        alert_indicator_threshold: '{{.Values.alertRules.workload.pod.status.phase.not.running.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: workload.pod.status.phase.not.running-43b6y
        alert_project: '{{.Values.global.project}}'
        severity: Medium
    - alert: workload.replicas.available-42c6y-39664e7ab8c259e45e8ccd8376f46964
      annotations:
        alert_current_value: '{{ `{{$value}}` }}'
        alert_notifications: '[{"namespace":"{{.Values.global.namespace}}","name":"{{.Values.global.adminNotification}}"}]'
      expr: min(kube_deployment_status_replicas_available{deployment="juno",namespace="{{.Values.global.namespace}}"})<{{.Values.alertRules.workload.replicas.available.threshold}}
      for: {{ .Values.alertRules.workload.rules.for }}
      labels:
        alert_cluster: '{{.Values.global.cluster.name}}'
        alert_resource: cpaas-juno-rules
        alert_indicator: workload.replicas.available
        alert_indicator_aggregate_range: '0'
        alert_indicator_comparison: <
        alert_indicator_threshold: '{{.Values.alertRules.workload.replicas.available.threshold}}'
        alert_involved_object_kind: Deployment
        alert_involved_object_name: juno
        alert_involved_object_namespace: '{{.Values.global.namespace}}'
        alert_name: workload.replicas.available-42c6y
        alert_project: '{{.Values.global.project}}'
        severity: Medium
{{- end }}
