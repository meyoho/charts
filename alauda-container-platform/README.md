
archon:
  multiClusterHost: https://erebus    # erebus地址，注意是https的
  labelBaseDomain: alauda.io          # label 和 annotation 里的域名

icarus:
  gateway: https://129.28.182.197     # API Gateway 的地址，目前必须是 HTTPS 的